/* Distributed under the OSI-approved BSD 3-Clause License.  See accompanying
   file Copyright.txt                                                      */

#ifndef TEST2_HPP
#define TEST2_HPP
#include <string>
#include <chrono>
#include <tuple>

namespace named {

class cross_target
{
public:
   double found_var = 1;
   std::string found_string = "CrossTarget";
};

#ifdef __linux__
    typedef std::chrono::duration<long> minute_duration;
    typedef std::chrono::duration<long, std::ratio<60>> hour_duration;
#elif _WIN32
    // Needs to be int to properly wrap "%=" operator of duration.
    typedef std::chrono::duration<int, std::ratio<3600>> minute_duration;
    typedef std::chrono::duration<int, std::ratio<60>> hour_duration;
#endif

inline float outside_adder_2(float t1, float t2) { return t1 + t2; };
enum class namedType { One, Two, Three };
enum unscopedType {Four, Five, Six};
};

template<typename R>
void template_f(R s) { s++; }

template<typename R>
void template_f_multi_arg(R s) { s++; }

template<typename R, typename T>
void multi_template(R arg1, T arg2) { arg1++; }

inline int overloaded_non_template() { return 0; }

inline int overloaded_non_template(int arg1) { return arg1; }

inline int overloaded_non_template(int arg1, int arg2) { return arg1 + arg2; }

inline float no_namespace_adder(float t1 = 0, float t2 = 1) { return t1 + t2; }

#endif
