# Distributed under the OSI-approved BSD 3-Clause License.  See accompanying
# file Copyright.txt

import unittest
import skiplistTest
from skiplistTest.simple import SimpleClass as sc
from skiplistTest.simple import SimpleClass as secondC


class Testskiplist(unittest.TestCase):
    def test_basic_constructor(self):
        sc()
        sc(1, "hello world")
        secondC(1, "Second world")

    def test_skiplist(self):
        s = dir(sc())
        self.assertNotIn("external_str", s)
        self.assertNotIn("external_int", s)
        self.assertNotIn("wrong_one", s)
        self.assertNotIn("notFound", s)
        self.assertNotIn("im_protected_and_excluded", s)
        self.assertIn("public_member", s)
        self.assertNotIn("__call__", s)
        self.assertNotIn("__sub__", s)
        self.assertIn("__neg__", s)

    def test_second_skiplist(self):
        s = dir(secondC())
        self.assertNotIn("external_int", s)

    def test_basic_methods_and_members(self):
        cl = sc(1, "frame1")
        self.assertEqual("frame1", cl.get_str_val())
        self.assertEqual(1, cl.get_int_val())
        cl.print_internals()
        self.assertEqual(12, cl.public_member)
        cl.set_int_val(2)
        cl.set_str_val("bird")
        self.assertEqual("bird", cl.get_str_val())
        self.assertEqual(2, cl.get_int_val())


if __name__ == "__main__":
    unittest.main()
