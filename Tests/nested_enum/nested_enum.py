import nested_enum_module

s = nested_enum_module.simple()

print(s.values)
print(s.values.One)
print(s.values.val2)

r = s.hello()
print("hello returned " + str(r) + "\n")
if r == 10:
    exit(0)
else:
    exit(-1)
