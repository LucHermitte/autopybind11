#ifndef SHARED_HOLDER
#define SHARED_HOLDER

#include <memory>
#include <vector>
#include "shared_base.hpp"


typedef std::shared_ptr<std::vector<double>> sv_doub;
using s_int = std::shared_ptr<int>;
class sharedHolder
{
public:
    sharedHolder();
    sharedHolder(std::shared_ptr<Base> b);
    const int sum();
    const int size();
    std::shared_ptr<Base> base_arr();
    std::shared_ptr<std::vector<double>> get_vec();
    void set_vec(std::shared_ptr<std::vector<double>> new_v);
    sv_doub ret_vec(sv_doub a) {return a;}
    s_int ret_int(s_int a) {return a;}

private:
    std::shared_ptr<Base> b;
    std::shared_ptr<std::vector<double>> v;
};

#endif // SHARED_HOLDER