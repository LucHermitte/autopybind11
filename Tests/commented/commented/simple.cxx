#include <iostream>
#include "simple.h"

simple::simple()
{
}

/// cxx comment
/// with multiple lines
int simple::hello()
{
  std::cout << "hello\n";
  return 10;
}

void simple::invisible()
{
  std::cout << "I'm not here\n";
}

void simple::doubleLine()
{
  std::cout << "or here\n";
}

std::shared_ptr<std::vector<double>> simple::get_vec()
{
    return this->v;
}