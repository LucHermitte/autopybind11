#ifndef simple_h
#define simple_h

#include <memory>
#include <vector>

/** class comment */
class uSimple
{
public:
/** function comment
    with multiple lines */
  uSimple();
  /** method comment */
  int hello();
  /*! variable comment
    with multiple lines */
  int field_obj = 1;

  /*! unique holder comment
  across multiple lines */
  std::shared_ptr<std::vector<double>> get_vec();

  /** enum comment */
  enum Values {
    Val0,
    Val1
  };

protected:
    // Non-doc comment before.
    /** doc comment */
    // Non-doc comment after.
    void invisible();

    /** Still need a comment
    on multiple lines */
    void doubleLine();
    std::shared_ptr<std::vector<double>> v;
};

#endif
