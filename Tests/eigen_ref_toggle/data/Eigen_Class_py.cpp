#include <pybind11/pybind11.h>
#include <pybind11/iostream.h>
#include <pybind11/stl.h>
#include "eigen_no_ref.hpp"
#include <pybind11/eigen.h>






namespace py = pybind11;
void apb11_toggle_eigen_ref_Eigen_Class_py_register(py::module &m)
{
  py::class_<::Eigen_Class> PyEigen_Class(m, "Eigen_Class");
        
    
    PyEigen_Class.def(py::init<::Eigen_Class const &>(),py::arg("arg0"))
    .def(py::init<>())
    .def("getGivenMat", static_cast<::Eigen::MatrixXd ( ::Eigen_Class::* )( ::Eigen::MatrixXd const )>(&::Eigen_Class::getGivenMat), py::arg("mat"))
    .def("getMatrix", static_cast<::Eigen::MatrixXd & ( ::Eigen_Class::* )(  )>(&::Eigen_Class::getMatrix))
    .def("scale_by_2", static_cast<void ( ::Eigen_Class::* )( ::Eigen::VectorXd )>(&::Eigen_Class::scale_by_2), py::arg("v"))
    .def("viewMatrix", static_cast<::Eigen::MatrixXd const & ( ::Eigen_Class::* )(  )>(&::Eigen_Class::viewMatrix))
    
    
    ;
}
