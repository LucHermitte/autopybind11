import classAttributesTest
import unittest


class test_class_attributes(unittest.TestCase):
    def test_basic(self):
        s = classAttributesTest.simple()
        r = s.hello()
        self.assertEqual(r, 10)
        self.assertEqual(s.kNumChannels, 2)
        self.assertEqual(s.kValue, 0)

        print(s)
        print(dir(s))


if __name__ == "__main__":
    unittest.main()
