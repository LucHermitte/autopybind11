#include <iostream>
#include "simple.h"

simple::simple()
{
}
simple::simple(int var1, int optional)
{
  std::cout << optional << "\n";
}
simple::simple(int var1, char optional, double opt2)
{
  std::cout << optional << "\n";
}

int simple::hello()
{
  std::cout << "hello\n";
  return 10;
}

void outside()
{
  std::cout << "Not inside\n" ;
}