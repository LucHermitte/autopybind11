/* Distributed under the OSI-approved BSD 3-Clause License.  See accompanying
   file Copyright.txt                                                      */

#ifndef BASE_HPP
#define BASE_HPP
#include <string>


inline float free_add(float t1, float t2) { return t1 + t2; }
template<typename T> 
inline T free_add_templ(T t1, T t2)
{ 
  return t1 + t2; 
}
enum class namedType { One, Two, Three };
enum unscopedType {Four, Five, Six};



namespace nmspc1 {
class Base
{
public:
  inline Base() {}
  inline virtual std::string test_() {return name;} 
  inline int base_adder(const int& val) {return base_sum + val; }

  enum Vals {
    val1,
    val2
  };

private:
  const int base_sum = 0;
  const std::string name = "Base";
  
   
};
}

#endif