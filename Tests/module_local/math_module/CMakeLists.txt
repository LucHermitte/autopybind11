add_library(math_module SHARED ${CMAKE_CURRENT_SOURCE_DIR}/mather.cxx)
target_include_directories(math_module PRIVATE ${CMAKE_CURRENT_SOURCE_DIR})

autopybind11_add_module("math_mod"
                       YAML_INPUT ${CMAKE_CURRENT_SOURCE_DIR}/wrapper_input.yml
                       CONFIG_INPUT ${CMAKE_CURRENT_SOURCE_DIR}/config.yml
                       DESTINATION ${CMAKE_CURRENT_BINARY_DIR}
                       LINK_LIBRARIES math_module
                       )