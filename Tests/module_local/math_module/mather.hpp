/* Distributed under the OSI-approved BSD 3-Clause License.  See accompanying
   file Copyright.txt      */
#ifndef MATHER_HPP
#define MATHER_HPP

namespace mather {

    class multy
    {
    public:
        multy() : val1(0), val2(0) {};
        multy(int i,int j);
        void setter(int i, int j);
        int multiplier();
    private:
        int val1;
        int val2;
    };

}

class adder
    {
    public:
        adder() : val1(0), val2(0) {};
        adder(int i, int j);
        void setter(int i, int j);
        int sum();
    private:
        int val1;
        int val2;
    };

#endif //MATHER_HPP