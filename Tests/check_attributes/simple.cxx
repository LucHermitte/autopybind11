#include <iostream>
#include "simple.h"

simple::simple()
{
}

int simple::hello()
{
  std::cout << "hello\n";
  return 10;
}

int simple::one()
{
  std::cout << "hello\n";
  return 10;
}

int simple::two() const
{
  std::cout << "hello\n";
  return 10;
}

int simple_2::three()
{
  std::cout << "hello\n";
  return 0;
}
