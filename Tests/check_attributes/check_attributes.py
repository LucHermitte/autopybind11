
# Distributed under the OSI-approved BSD 3-Clause License.  See accompanying
# file Copyright.txt

import os
import sys
import unittest

# Make it so we search where we are running.
sys.path.append(os.getcwd())
import check_attributesModule



class checkAttributesTest(unittest.TestCase):

    def testCheckSimple_base(self):
        s = check_attributesModule.simple_base()
        # Pure virtual class, no objects to run
        self.assertNotIn("hello", dir(s))

    def testCheckSimple(self):
        # Contains new functions
        s = check_attributesModule.simple()
        self.assertIn("one", dir(s))
        self.assertIn("two", dir(s))
        self.assertIn("three", dir(s))
        # And the implementation of hello
        self.assertIn("hello", dir(s))
        self.assertEqual(s.one(), 10)
        self.assertEqual(s.two(), 10)

    def testCheckSimple_2(self):
        s = check_attributesModule.simple_2()
        # Protected functions not found in this instance
        self.assertNotIn("one", dir(s))
        self.assertNotIn("two", dir(s))

        # implementation of three
        self.assertIn("three", dir(s))
        self.assertEqual(s.three(), 0)

        # Since "simple" marked "hello" as final, hello better not be here
        self.assertNotIn("hello", dir(s))

if __name__ == '__main__':
    unittest.main()
