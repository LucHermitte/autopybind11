#ifndef simple_h
#define simple_h

class simple_base
{
  virtual int hello() = 0;
};

class simple :simple_base
{
public:
  simple();
  int hello() final ;
protected:
  int one();
  int two() const;
  virtual int three() = 0;
};

class simple_2 : simple
{
  protected:
    int three() final;
};
#endif
