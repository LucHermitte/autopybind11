#ifndef simple_template
#define simple_template

#include <string>
#include "basic_enum.h"

class simple_tag
{
public:
    simple_tag() {}
    simple_tag(std::string const & t_name) : tag_name(t_name) {}
    std::string get_name() {return this->tag_name;}
    virtual bool is_tag(tag t) {return true;}
private:
    const std::string tag_name;
};

template<tag T>
class Tagged
{
public:
    Tagged(): my_tag(T){}
    Tagged(std::string &s) : name(s), my_tag(T){}
private:
    tag my_tag;
    std::string name;
};

#endif // simple_template