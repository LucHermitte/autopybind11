import os, sys, unittest

from enum_dep import (
    simple_tag as st,
    tag,
    Tagged_TAG_A as ttA,
    Tagged_TAG_C as ttC,
    Tagged_TAG_B as ttB,
)


class TestSimpleTag(unittest.TestCase):
    def test_constructor(self):
        st()
        st("Tag A")

    def test_methods(self):
        s = st("Tag A")
        self.assertEqual(s.get_name(), "Tag A")
        self.assertTrue(s.is_tag(tag.TAG_A))


class TestTagged(unittest.TestCase):
    def test_constructor(self):
        ttA()
        ttB()
        ttC()

        ttA("Tag A")
        ttB("Tag B")
        ttC("Tag C")


if __name__ == "__main__":
    unittest.main()
