/* Distributed under the OSI-approved BSD 3-Clause License.  See accompanying
   file Copyright.txt                                                      */

#ifndef TMPLT_ABSTRACT_DERIVED1_HPP
#define TMPLT_ABSTRACT_DERIVED1_HPP

#include "non_template/base.hpp"

namespace nmspc1 {

// This class is only really going to be used
// to test the behavior of pure virtual functions
// that are transparently inherited for templated classes
template <typename T>
class TAD1: public Base {};
}
#endif