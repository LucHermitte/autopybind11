// Distributed under the OSI-approved BSD 3-Clause License.  See accompanying
// file Copyright.txt

#ifndef SECOND_TARGET_SOURCE_HPP
#define SECOND_TARGET_SOURCE_HPP

class second_target_class
{
public:
  second_target_class() = default;

  #ifdef FLAG2
    void flag2_defined(){}
  #else
    void flag2_not_defined(){}
  #endif

  #ifdef FLAG3
    void flag3_defined(){}
  #else
    void flag3_not_defined(){}
  #endif
};

#endif