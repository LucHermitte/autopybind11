
#include "simple.h"

simple::simple()
{
}

int simple::hello()
{
  std::cout << "hello\n";
  return 10;
}

advanced::advanced()
{
}

void advanced::hello(std::string name)
{
  std::cout << "hello" <<name << "\n";
}