import os, sys, unittest

from container_mod import Container, Simple_Int


class TestContainer(unittest.TestCase):
    def test_container_methods(self):
        c = Container()
        i = c.at(2)
        i_2 = c.ret(Simple_Int(2))
        self.assertEqual(i.get(), i_2.get())
        self.assertEqual(c.size(), 5)


if __name__ == "__main__":
    unittest.main()
