# Distributed under the OSI-approved BSD 3-Clause License.  See accompanying
# file Copyright.txt

import os
import sys
import unittest

# Make it so we search where we are running.
sys.path.append(os.getcwd())

from mixed import custom_mixed_namespace_module as cmn
from pure_python import custom_python_namespace_module as cp



class customNamespaceTests(unittest.TestCase):

    def drive_test_module_structure(self, module_list, dir_list):
        for mod, dir_ in zip(module_list, dir_list):
            self.assertSetEqual(dir_, (set(dir(mod)) & dir_))

    def drive_test_constructors(self, class_name):
        return class_name()

    def drive_test_methods(self, class_name):
        test_class = self.drive_test_constructors(class_name)
        test_class.decrement_vars(1, 1.0)
        self.assertEqual(-2, test_class.sum_vars())
        test_class.increment_vars(10, 0)
        self.assertEqual(8, test_class.sum_vars())

    def drive_test_class(self, class_names):
        for name in class_names:
            self.drive_test_methods(name)

    def drive_test_free_functions(self, free_fun):
        self.assertEqual(2, free_fun(1, 1))

    def test_mixed_namespace(self):
        mod_list = [cmn, cmn.outer, cmn.outer.inbetween,
                    cmn.outer.inbetween.outer_but_inner,
                    cmn.outer.inbetween.justafunc,
                    cmn.outer.inbetween.three, cmn.foo, cmn.foo.bar,
                    cmn.foo.bar.third]
        mos_struc_validate = [set(['foo','outer']),set(['inbetween']),
                              set(['justafunc','outer_but_inner']),
                              set(['outer_but_inner_class']),
                              set([]), set(['third_namespace_fun']),
                              set(['bar', 'no_nmspc_class', 'no_nmspc_func']),
                              set(['getvalue','inbetweener',
                              'inbetweener_fun', 'outer_namespace_fun', 'third']),
                              set(['third_nmspc_class'])]
        self.drive_test_module_structure(mod_list, mos_struc_validate)
        self.drive_test_class([cmn.foo.no_nmspc_class])
        self.drive_test_class([cmn.outer.inbetween.outer_but_inner.outer_but_inner_class])
        self.drive_test_class([cmn.foo.bar.inbetweener])
        self.drive_test_class([cmn.foo.bar.third.third_nmspc_class])
        self.drive_test_free_functions(cmn.foo.no_nmspc_func)
        self.drive_test_free_functions(cmn.outer.inbetween.three.third_namespace_fun)
        self.drive_test_free_functions(cmn.foo.bar.inbetweener_fun)
        self.drive_test_free_functions(cmn.foo.bar.outer_namespace_fun)

    def test_pure_python_namespace(self):
        mods_set = [cp,cp.foo,cp.foo.bar,cp.foo.bar.third]
        mods_struc_validate = [set(['foo','outer_but_inner_class','outer_namespace_fun']),
                                set(['bar','no_nmspc_func','no_nmspc_class']),
                                set(['getvalue', 'inbetweener_fun',
                                 'third', 'inbetweener']),
                                set(['third_namespace_fun', 'third_nmspc_class'])]
        self.drive_test_module_structure(mods_set, mods_struc_validate)
        class_names_cp = [cp.outer_but_inner_class]
        class_names_foo = [cp.foo.no_nmspc_class]
        class_names_bar = [cp.foo.bar.inbetweener]
        class_names_third = [cp.foo.bar.third.third_nmspc_class]
        self.drive_test_class(class_names_cp)
        self.drive_test_class(class_names_foo)
        self.drive_test_class(class_names_bar)
        self.drive_test_class(class_names_third)
        self.drive_test_free_functions(cp.outer_namespace_fun)
        self.drive_test_free_functions(cp.foo.no_nmspc_func)
        self.drive_test_free_functions(cp.foo.bar.inbetweener_fun)
        self.drive_test_free_functions(cp.foo.bar.third.third_namespace_fun)

if __name__ == '__main__':
    unittest.main()