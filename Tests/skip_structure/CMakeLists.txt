cmake_minimum_required(VERSION 3.15)
project(skip_structure CXX)

find_package(AutoPyBind11)
autopybind11_fetch_build_pybind11(PYBIND11_DIR ${PYBIND11_SRC_DIR})

add_subdirectory(no_structure)
add_subdirectory(structure)
add_subdirectory(empty_structure)
