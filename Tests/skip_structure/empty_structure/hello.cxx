// Distributed under the OSI-approved BSD 3-Clause License.  See accompanying
// file Copyright.txt
#include "hello.h"

std::string outer::simple::hello()
{
    return std::string("Hello World");
};
