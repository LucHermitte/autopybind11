import simpleTest
import sys
import unittest

s = simpleTest.simple()
s2 = simpleTest.simple(data=4)


class OperatorTestSuite(unittest.TestCase):
    def test_simple(self):
        r = s.hello()
        self.assertEqual(r, 10)

    def test_simpleData(self):
        self.assertRaises(TypeError, simpleTest.simple, val=4)
        self.assertRaises(TypeError, simpleTest.simple, "3")
        r2 = s2.hello()
        self.assertEqual(r2, 10)

        r3 = s2.hello(22.0)
        self.assertRaises(TypeError, s2.hello, "22")
        self.assertEqual(r3, 22.0)

        r4 = s2.hello(3.0)
        self.assertEqual(r4, 3.0)

    def test_simpleAlias(self):
        alias = simpleTest.aliasClass_simple()
        self.assertEqual(alias.simple(), 12)
        self.assertIsInstance(alias.getTemplate(), simpleTest.simple)

    def test_simple2(self):
        simpleTest.simple2_simple_aliasClass_basic()
        simpleTest.simple2_simple_aliasClass_double()
        simpleTest.simple2_simple_aliasClass_new_alias()


if __name__ == "__main__":
    testRunner = unittest.main(__name__, argv=["main"], exit=False)
    if not testRunner.result.wasSuccessful():
        sys.exit(1)
