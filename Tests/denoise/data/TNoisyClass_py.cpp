#include <pybind11/pybind11.h>
#include <pybind11/iostream.h>
#include <pybind11/stl.h>
#include "noisey.hpp"
#include <pybind11/eigen.h>
#include <pybind11/eigen.h>
#include "C:\apb_fork\src\Tests\denoise\defs.h"






namespace py = pybind11;


template <typename Class, typename... Options>
py::class_<Class, Options...> DefineTemplateClass(
    py::handle scope, const char* name,
    const char* doc_string = "") {
  py::class_<Class, Options...> py_class(
      scope, name, doc_string);
  return py_class;
};

void apb11_denoise_mod_TNoisyClass_py_register(py::module &m)
{
  // Instantiation of TNoisyClass<Eigen::Matrix3d>
  py::class_<TNoisyClass<Eigen::Matrix3d>> PyTNoisyClass_Matrix3d(m, "TNoisyClass_Matrix3d");
        
    
    PyTNoisyClass_Matrix3d.def(py::init<>())
    .def(py::init<TNoisyClass<Eigen::Matrix3d> const &>(),py::arg("arg0"))
    
    
    
    ;



  // Instantiation of TNoisyClass<Eigen::Vector4f>
  py::class_<TNoisyClass<Eigen::Vector4f>> PyTNoisyClass_Vector4f(m, "TNoisyClass_Vector4f");
        
    
    PyTNoisyClass_Vector4f.def(py::init<>())
    .def(py::init<TNoisyClass<Eigen::Vector4f> const &>(),py::arg("arg0"))
    
    
    
    ;



  // Instantiation of TNoisyClass<Coupler<Mid>>
  py::class_<TNoisyClass<Coupler<Mid>>> PyTNoisyClass_MidCouple(m, "TNoisyClass_MidCouple");
        
    
    PyTNoisyClass_MidCouple.def(py::init<>())
    .def(py::init<TNoisyClass<Coupler<Mid>> const &>(),py::arg("arg0"))
    
    
    
    ;
}
