#ifndef NOISY
#define NOISY

#include "defs.h"
#include <Eigen/Dense>
#include <list>
#include <vector>
using namespace First;
class NoisyClass
{
public:
    NoisyClass() {}
    void noisyStd(std::list<MidCouple> m = std::list<MidCouple>(), std::vector<int> vec = std::vector<int>()) {}
    Eigen::Ref<RowMatrixXd> noisyEigen(RowMatrixXd m) {return Eigen::Ref<RowMatrixXd>(m);}
};

template <typename T = Coupler<>>
class TNoisyClass
{
public:
    TNoisyClass() {}
};

#endif
