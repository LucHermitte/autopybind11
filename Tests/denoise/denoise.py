import os, sys, unittest, re

sys.path.append(os.getcwd())


def import_denoise():
    import denoise_mod


class DenoiseTest(unittest.TestCase):

    # Taken from smoke_test.py
    def whitespace_normalize(self, s):
        # Naive and ugly, but gets the job done.
        return s.replace(" ", "").replace("\n", "").replace("/", "\\")

    def remove_includes(self, in_str):
        pat = r"#include.+"
        repl = ""
        return re.sub(pat, repl, in_str)

    @unittest.expectedFailure
    def test_Import(self):
        # No module means an import error is thrown.
        self.assertRaises(ImportError, import_denoise)

    def test_Construct(self):
        import denoise_mod

        denoise_mod.NoisyClass()
        denoise_mod.TNoisyClass_MidCouple()
        denoise_mod.TNoisyClass_Vector4f()
        denoise_mod.TNoisyClass_Matrix3d()

    def test_Normalization(self):
        for filename in ["NoisyClass_py.cpp", "TNoisyClass_py.cpp"]:
            with open(filename, "r") as f:
                with open("data/" + filename, "r") as df:
                    expected = self.remove_includes(df.read())
                    expected = self.whitespace_normalize(expected)
                    in_text = self.remove_includes(f.read())
                    in_text = self.whitespace_normalize(in_text)
                    self.assertEqual(expected, in_text)


if __name__ == "__main__":
    unittest.main()
