#ifndef SHARED_BASE
#define SHARED_BASE

#include <memory>
class Base:
public std::enable_shared_from_this<Base>
{
public:
    Base() {this->size_ = 1;
            this->arr = new int[1]{0};}
    Base(size_t size, int value = 0) : size_(size) { arr = new int[size]{value}; }
    ~Base() {delete arr;}
    inline int const get_arr() const {return *arr;}
    inline size_t const size() const {return size_;}
    inline const int sum() const {int sum = 0;
                                  for(int i = 0; i<this->size_;i++)
                                  {
                                    sum = sum + *(this->arr+i);
                                  }
                                  return sum;}

private:
    int * arr;
    size_t size_;
};

#endif // SHARED_BASE