#ifndef DERIVED_H
#define DERIVED_H

#include "base.hpp"

class Derived : public Base
{
public:
    Derived(int d) {var_base = d;}
    Derived( Derived const& d) {this->var_base = d.var_base;}
    virtual inline int get_var() {return var_base;}
    virtual inline std::string hello() { return "Hello World";}
    inline int get_answer() {return 42;}
protected:
};


#endif // DERIVED_H