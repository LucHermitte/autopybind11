# Distributed under the OSI-approved BSD 3-Clause License.  See accompanying
# file Copyright.txt
cmake_minimum_required(VERSION 3.15)
project(virtuality CXX)

find_package(AutoPyBind11)

autopybind11_fetch_build_pybind11(PYBIND11_DIR ${PYBIND11_SRC_DIR})

add_library(virtuality INTERFACE)
target_sources(virtuality INTERFACE
               ${CMAKE_CURRENT_SOURCE_DIR}/base.hpp
               ${CMAKE_CURRENT_SOURCE_DIR}/derived.hpp
               )

target_include_directories(virtuality INTERFACE ${CMAKE_CURRENT_SOURCE_DIR})

autopybind11_add_module("virtuality_module"
                       YAML_INPUT ${CMAKE_CURRENT_SOURCE_DIR}/wrapper_input.yml
                       CONFIG_INPUT ${CMAKE_CURRENT_SOURCE_DIR}/config.yml
                       DESTINATION ${CMAKE_CURRENT_BINARY_DIR}
                       LINK_LIBRARIES virtuality
                       )
