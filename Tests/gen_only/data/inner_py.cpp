#include <pybind11/pybind11.h>
#include <pybind11/iostream.h>
#include <pybind11/stl.h>
#include "inner.hpp"

namespace py = pybind11;

py::module apb11_gen_only_module_inner_py_register(py::module &m)
{
  using namespace inner;

  py::module Pyinner = m.def_submodule("inner", "");
  Pyinner.def("inner_fxn", static_cast<void (*)(  )>(&inner_fxn));
  return Pyinner;
}
