# Distributed under the OSI-approved BSD 3-Clause License.  See accompanying
# file Copyright.txt
cmake_minimum_required(VERSION 3.15)
project(gen_only_lib CXX)

find_package(AutoPyBind11)
autopybind11_fetch_build_pybind11(PYBIND11_DIR ${PYBIND11_SRC_DIR})

add_library(gen_only_lib INTERFACE)

target_sources(gen_only_lib INTERFACE
  ${CMAKE_CURRENT_SOURCE_DIR}/inner.hpp
  ${CMAKE_CURRENT_SOURCE_DIR}/outer.hpp)

target_include_directories(gen_only_lib INTERFACE
  ${CMAKE_CURRENT_SOURCE_DIR})

file(COPY data DESTINATION ${CMAKE_CURRENT_BINARY_DIR})

autopybind11_add_module(gen_only_module
                       YAML_INPUT ${CMAKE_CURRENT_SOURCE_DIR}/wrapper_input.yml
                       DESTINATION ${CMAKE_CURRENT_BINARY_DIR}
                       LINK_LIBRARIES gen_only_lib
                       GEN_ONLY TRUE
                       NO_FORMAT TRUE)

# To include files in a separate library, access the list of sources generated
# via the custom target of ${NAME}_files.
# The above module would be accessed by:
# get_target_property(GENERATED_FILES apb_gen_only_module_files SOURCES)