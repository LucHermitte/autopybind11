import simpleTest

s = simpleTest.simple()
r = s.hello()

s.print_four()
s.print_four([5, 6, 7, 8])
try:
    s.print_four([5, 6, 7])
except TypeError:
    print("print_four properly raised a TypeError after sending in 3 values")
try:
    s.print_one([5, 6, 7])
except TypeError:
    print(
        "print_one properly raised a TypeError after sending in a list of values"
    )
try:
    s.print_one("str")
except TypeError:
    print("print_one properly raised a TypeError after sending in a string")
s.print_one(1)

print("hello returned " + str(r) + "\n")
if r == 10:
    exit(0)
else:
    exit(-1)
