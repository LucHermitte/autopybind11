# Distributed under the OSI-approved BSD 3-Clause License.  See accompanying
# file Copyright.txt

"""
Separate file for the text blocks to be formatted.
These strings will be formatted in generator.py, then used to
generate valid pybind11 binding code
"""

submodule_signature = (
    """py::module {mod_name} = m.def_submodule("{name}", "");"""
)
arg_val_cast = " = {type}({val})"
nullptr_arg_val = " = ({type}){val}"
common_cpp_body = """
#include <pybind11/pybind11.h>
#include <pybind11/iostream.h>
#include <pybind11/stl.h>

namespace py = pybind11;

{forwards}
PYBIND11_EXPORT void apb11_{name}_register_types(py::module &model)
{{
// make sure this module is only initialized once
static bool called = false;
if(called) {{ return; }}
called = true;

// initialize class for module {name}
{init_funs}
}};

PYBIND11_MODULE({name}, model)
{{
// First initialize dependent modules
// then this module.
{autobind_calls}
}}

"""
forward_ending = """(py::module &model)"""
signature_ending = """(model)"""

init_fun_signature = """apb11_{{module}}_{{name}}_register{ending};\n""".format(
    ending=signature_ending
)
init_fun_forward = """void apb11_{{module}}_{{name}}_register{ending};\n""".format(
    ending=forward_ending
)
module_init_fun_signature = """apb11_{{module}}_register_types{ending};\n""".format(
    ending=signature_ending
)
module_init_fun_forward = """void apb11_{{module}}_register_types{ending};\n""".format(
    ending=forward_ending
)
submodule_init_fun_signature = (
    """apb11_{module}_{name}_register({module_var});\n"""
)
submodule_init_fun_forward = (
    """void apb11_{module}_{name}_register(py::module &{module_var});\n"""
)
init_fun_forward_non_void = """py::module apb11_{{module}}_{{name}}_register{ending};\n""".format(
    ending=forward_ending
)
submodule_init_fun_signature_return = (
    """auto {mod_name} = apb11_{module}_{name}_register({module_var});\n"""
)
submodule_init_fun_forward_non_void = """py::module& apb11_{module}_{name}_register(py::module &{module_var});\n"""
submodule_return = """return {name};"""

cppbody = """
#include "pybind11/operators.h"
#include "pybind11/pybind11.h"
#include <pybind11/iostream.h>
#include "pybind11/stl.h"

{includes}


namespace py = pybind11;
//#include "drake/bindings/pydrake/documentation_pybind.h"
//#include "drake/bindings/pydrake/pydrake_pybind.h"

// namespace drake {{
// namespace pydrake {{
PYBIND11_EXPORT void apb11_{name}_register_types(py::module &m)
{{
  {class_info}
}}

// }}  // namespace pydrake
// }}  // namespace drake
"""
module_local = """, py::module_local()"""

template_setup_fn = """
template <typename Class, typename... Options>
py::class_<Class, Options...> DefineTemplateClass(
    py::handle scope, const char* name,
    const char* doc_string = "") {
  py::class_<Class, Options...> py_class(
      scope, name, doc_string);
  return py_class;
};
"""
class_init_wrapper = """auto {mod_name} = DefineTemplateClass<{pyclass_args}>(m,"{name}"{doc});"""
class_init_call = """py::class_<{pyclass_args}{no_delete}> {mod_name}(m, "{name}"{doc}{mod_loc});"""
class_info_body = """
  {tmplt_note}
  {class_cmd}
    {attrs}
    {enums}
    {mod_name}{constructor}
    {funcs}
    {vars}
    {opers}
    ;
"""
pybind_overload_macro_args = """localType,
      {parent_alias},
      {cpp_fxn_name},
      {arg_str}
"""

tramp_override = """{fxn_sig} override
  {{
    using localType = {return_type};
    PYBIND11_OVERLOAD{pure}
    (
      {macro_args}
    );
  }}"""

trampoline_def = """class {tramp_name}
: public {class_decl}
{{
public:
  typedef {class_decl} {parent_alias};
  using {parent_alias}::{ctor_name};

  {virtual_overrides}
}};
"""

publicist_using_directives = """using {class_decl}::{fxn_name};"""

publicist_def = """class {publicist_name}
: public {class_decl}
{{
public:
  {using_directives}
}};
"""

class_module_cpp = """#include <pybind11/pybind11.h>
#include <pybind11/iostream.h>
#include <pybind11/stl.h>
{includes}


{namespace_str}
{trampoline_str}
{publicist_str}
namespace py = pybind11;

{tmplt_fn}
void apb11_{module}_{namespace}_register(py::module &m)
{{
  {defs}
}}
"""

non_class_module_cpp = """#include <pybind11/pybind11.h>
#include <pybind11/iostream.h>
#include <pybind11/stl.h>
{includes}
namespace py = pybind11;
{forwards}
void apb11_{module}_{namespace}_register(py::module &m)
{{

  {namespace_str}
  {defs}
}}
"""

non_class_module_return_cpp = """#include <pybind11/pybind11.h>
#include <pybind11/iostream.h>
#include <pybind11/stl.h>
{includes}
namespace py = pybind11;
{forwards}
py::module apb11_{module}_{namespace}_register(py::module &m)
{{
  {namespace_str}
  {defs}
  return {mod_name};
}}
"""

member_func = """{module}.def{static}(\"{fun_name}\", {fun_ref}{args}{doc}{keepalive}{return_policy}){ending}\n"""

constructor = """.def(py::init<{}>(){}{}{})\n"""
constructor_generic = """.def(py::init({init_body}))"""
generic_lambda = """[{state}]({args}){{
  {body}
}}"""
custom_constructor = """.def(py::init({cust_ctor}))\n"""
copy_constructor_tramp = """.def(py::init([]({tramp_arg_type} arg0) {{ return {trampname}(arg0); }}){doc})
{indent}.def(py::init([]({arg_type} arg0) {{ return {cname}(arg0); }}))\n"""
member_func_arg = """py::arg(\"{}\"){}"""
public_member_var = """.def_read{write}{static}(\"{var_name}\", {var_ref})\n"""
private_member_var = (
    """.def_property{readonly}{static}(\"{var_name}\", {var_accessors})\n"""
)
member_reference = "&{classname}::{member}"
overload_template = """static_cast<{decl_string}>({fun_ref})"""
operator_template = """.def({arg1} {symbol} {arg2})\n"""
call_template = (
    """.def("{py_name}", +[]({arg_str}){{ return {fn_call}; }})\n"""
)

wrap_header = """{includes}\n{class_decs}\n{func_ptr_assigns}"""
enum_header = """py::enum_<{class_name}>({module},\"{name}\",{type}{doc})\n"""
enum_val = """.value(\"{short_name}\", {scoped_name}, \"{doc}\")\n"""
keep_alive = """, py::keep_alive<{}, {}>()"""

# This is seemingly necessary.  Friend operators still run.
# See operators tests
repr_function = """.def("__str__", +[]({arg_type}) {{
        std::{stream_type}stringstream oss;
        oss {symbol} {arg_name};
        std::string s(oss.str());

        return s;}})"""

generic_lambda_def = """{module}.def("{name}", [{state}]({sig})
{{
  {body}
}}{return_policy}){ending}\n"""

return_policy = """, py::return_value_policy::{policy}"""

matrixDRef = """Eigen::Ref<{matrix_type}, 0, Eigen::Stride<Eigen::Dynamic, Eigen::Dynamic>> {ref}"""
shared_holder = """std::shared_ptr<{}>"""
unique_holder = """std::unique_ptr<{}>"""

member_lambda = """.def([{state}]({arg_list})
{{
  {body}
  {return_}
}})"""

generic_lambda = """[{state}]({signature}){{{body}{return_}}}"""
generic_return = """{return} {statement};"""
conversion = """auto wrap_{conv_name} = {converting_constr};"""

std_move = """std::move({})"""
make_shared = """std::make_shared<{type}>({var})"""
make_unique = """std::make_unique<{type}>({var})"""
compose_unique = """std::unique_ptr<{type}>(new {type}({var}))"""
make_tuple = """std::make_tuple({})"""

attribute = """{name}.attr("{attribute}") = {value}; """
