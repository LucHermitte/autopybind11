# Distributed under the OSI-approved BSD 3-Clause License.  See accompanying
# file Copyright.txt

import copy
import distutils.util
import fileinput
import glob
import json
import logging
import os
import subprocess
import re
import posixpath

import warnings
from configargparse import ArgParser, YAMLConfigFileParser
import pygccxml
import pygccxml.declarations as dec
import yaml

from autopybind11.bindings_sorter import get_binding_order
from autopybind11.op_names import names_dict, arg_dependent_ops
import autopybind11.text_blocks as tb
import autopybind11.customizer as customizer
from autopybind11._formatting import maybe_format_file_in_place


class BindingsGenerator:
    def __init__(self, opts, starting_indent=""):
        self.opts = opts
        self.indent = starting_indent
        self.name_data = {}
        self.data_web = {}
        self.name_tree = {}
        self.name_tree[self.opts.module_name] = {}
        self.skip_list = set()
        self.alias_data = {}
        # Dictionary to maintain a namespace -> auto_bind pointer for sub namespaces
        self.namespace_web = {}
        self.customizer = customizer.Customizer(
            self.name_tree[self.opts.module_name]
        )
        self.cns_flag = False
        self.validation_filters = []
        self.load_validation_filters()

    def load_mdx(self, file_name):
        with open(file_name, "r") as mdx_file:
            mdx_data = json.load(mdx_file)
            for full_name, auto_data_fnc in mdx_data.items():
                if full_name in self.data_web:
                    print(
                        "Error: attempting to capture two objects with the same name."
                    )
                    print(
                        "%s from %s and %s from %s"
                        % (
                            full_name,
                            auto_data_fnc,
                            full_name,
                            self.data_web[full_name],
                        )
                    )
                self.data_web[full_name] = auto_data_fnc

    def flatten(self, d_in, flat):
        for key in d_in.keys():
            flat.add(key)
            if type(d_in[key]) is dict:
                self.flatten(d_in[key], flat)

    def write_data_to_file(self, file_name, string):
        """
        Writes incoming strings to an output file, globally keep track of objects written?
        :param file_name: name of file to write to
        :param string: string to write out
        :return: None
        """
        assert file_name.endswith(".cpp"), cpp_file
        with open(file_name, "w") as cpp_file:
            cpp_file.write(string)
        if not self.opts.skip_formatting:
            maybe_format_file_in_place(file_name)

    def get_include_str(self, found_includes):
        include_str = ""
        for f in found_includes:
            if "#include" in f:
                include_str += f
            else:
                include_str += '#include "%s"\n' % f
        return include_str

    def normalize(self, file_content):
        if not self.opts.gns_flag:
            file_content = self.format_remove_global_namespace(file_content)
        if not self.opts.stdexdec_flag:
            file_content = self.stl_replace_partial_decl(file_content)
        if self.opts.td:
            file_content = self.remove_templ_default(file_content)
        if self.opts.aq:
            for nmspc in self.opts.aq:
                file_content = self.remove_assumed_nmspc(file_content, nmspc)
        return file_content

    def get_std_namespace(self):
        return self.name_data.namespace("std")

    def stl_replace_partial_decl(self, binding_str):
        std = self.get_std_namespace()
        for x_decl in std.declarations:
            if x_decl.decl_string.strip("::") in binding_str:
                binding_str = binding_str.replace(
                    x_decl.decl_string.strip("::"),
                    x_decl.partial_decl_string.strip("::"),
                )
        return binding_str

    def get_eigen_namespace(self):
        return self.name_data.namespace("Eigen")

    def remove_templ_default(self, binding_str):
        for default in self.opts.td:
            templ_type, default_types = default.split("|")
            default_types = default_types.split(",")
            for match in re.finditer(templ_type, binding_str):
                end_idx = self.extract_templ_indicies_rec(
                    match.end() + 1, binding_str
                )
                strt = match.start()
                edx = end_idx + 1
                templ_str = binding_str[strt:edx]
                templ_lst = list(
                    filter(
                        self.templ_default_filter,
                        zip(dec.templates.split(templ_str)[1], default_types),
                    )
                )
                templ_lst = [x[0] for x in templ_lst]
                templ_lst = self.sub_templ_insts(templ_lst)
                no_default_str = dec.templates.join(match.group(), templ_lst)
                binding_str = binding_str.replace(templ_str, no_default_str)
        return binding_str

    def templ_default_filter(self, inst):
        if inst[1] == "1":
            return True
        return False

    def format_remove_global_namespace(self, binding_member_str):
        pat = r"([\s.,<>()&*])::(\S*)"
        repl = r"\1\2"
        return self.reg_replace(pat, repl, binding_member_str)

    def remove_assumed_nmspc(self, binding_member_str, assumed_nmspc):
        pat = r":*%s:+" % assumed_nmspc
        repl = ""
        return self.reg_replace(pat, repl, binding_member_str)

    def reg_replace(self, pat, repl, str_):
        return re.sub(pat, repl, str_)

    def sub_templ_insts(self, templ_lst):
        templ_lst_ret = []
        for inst in templ_lst:
            templ_lst_ret.append(self.substitute_templ_insts(inst))
        return templ_lst_ret

    def substitute_templ_insts(self, templ_inst):
        try:
            typedef = self.name_data.decls(
                lambda x: x.decl_type.decl_string.strip("::") == templ_inst
                and type(x.parent) is dec.namespace_t,
                recursive=True,
                decl_type=dec.typedef_t,
            )
            if typedef:
                return typedef[0].name
        except pygccxml.declarations.runtime_errors.declaration_not_found_t:
            return templ_inst

    def generate_keep_alive_string(self, decl_data, keep_alive_data):
        num_args = len(decl_data.arguments)
        keep_alive_str = ""
        if decl_data.name in keep_alive_data:
            keep_alive_data = keep_alive_data[decl_data.name]
            if num_args in keep_alive_data["num_arguments"]:
                for pair_string in keep_alive_data["pairings"][
                    keep_alive_data["num_arguments"].index(num_args)
                ]:
                    data = pair_string.split(";")
                    keep_alive_str += self.opts.keep_alive_fmt.format(
                        data[0], data[1]
                    )
        return keep_alive_str

    def generate_enum_string(
        self, pybind_module_name, declaration, cpp_enum, enum_type=""
    ):
        cust_name = None
        custom_listing = {}
        if isinstance(declaration.parent, dec.class_declaration.class_t):
            custom_listing = self.customizer.get_custom_internal_enum_vals(
                cpp_enum, declaration.name
            )
            cust_name = self.customizer.get_custom_internal_enum_name(
                cpp_enum, declaration.name
            )
        else:
            custom_listing = self.customizer.get_custom_enum_vals(cpp_enum)
            cust_name = self.customizer.get_custom_name(cpp_enum)

        val_str = self.opts.enum_header_fmt.format(
            class_name=declaration.decl_string,
            module=pybind_module_name,
            name=declaration.name if cust_name is None else cust_name,
            type=enum_type if enum_type else "py::arithmetic()",
            doc=', \nR"""(%s)"""' % " \n".join(declaration.comment.text)
            if declaration.comment.text and self.opts.eds_flag
            else "",
        )
        # Check if YAML has custom names for enumerations

        for enum_obj in sorted(declaration.get_name2value_dict().keys()):
            scope_name = enum_obj
            if declaration.decl_string != "::":
                scope_name = "%s::%s" % (declaration.decl_string, enum_obj)
            if enum_obj in custom_listing:
                enum_obj = custom_listing[enum_obj]
            val_str += self.opts.enum_val_fmt.format(
                short_name=enum_obj, scoped_name=scope_name, doc=""
            )
        if self.customizer.get_custom_enum_export(cpp_enum):
            val_str += ".export_values()"
        val_str += ";\n"
        return val_str

    def check_protected_return_class(self, fn):
        search_data = self.name_data
        for (
            name_str
        ) in pygccxml.declarations.declaration_utils.declaration_path(fn):
            # Assume anything under std will isn't protected and don't descend further

            if name_str == "std":
                break
            # Accounts for the first level where name is likely "::"
            if name_str == search_data.name:
                continue
            # Else descend into the namespace to avoid recursive searching of
            # all name data
            if search_data.decls(name_str):
                search_data = search_data.decls(name_str)[0]
                continue
            # No more namespaces, lets look!
            decl = search_data.decl(name=name_str, type=type(fn))
            if self.protected_filter(decl):
                return True
        return False

    def wrap_eigen_args(self, arg):
        constructor_str = ""
        body_arg_str = ""
        arg_type = self.derive_base_from_compound_type(arg.decl_type)
        arg_dep = dec.type_traits.remove_declarated(arg_type)
        if self.is_eigen_matrix(arg_dep) and self.eigen_is_not_ref(arg_dep):
            # wrap in ref type
            type_checks = set((dec.cpptypes.reference_t, dec.cpptypes.const_t))
            traits = self.get_desired_type_traits(arg.decl_type, type_checks)

            constructor_str += self.add_eigen_ref_holder(
                arg.decl_type.decl_string,
                dec.cpptypes.const_t in traits,
                dec.cpptypes.reference_t in traits,
            )
            constructor_str += " " + arg.name + ","
            body_arg_str += arg.name + ", "
        return constructor_str, body_arg_str

    def generate_lambda_string(
        self,
        member_function=None,
        py_name="",
        module="",
        holder_function=None,
        added_functionality=None,
        body_method=None,
        return_value_policy="",
    ):
        """
        Takes a PyGCCXML function (member or otherwise) object and
        extracts the arguments to populate the constructor of an
        associated lambda function

        :param member_function: PyGCCXML data dictionary for individual function
        :param py_name: Name of the function on the python side. Defaults to the same
        name as the C++ function
        :param holder_function: Function pointer referencing function implementing the
        wrapping of an argument in a holder type such as std::shared_ptr, std::unique_ptr,
        or Eigen::Ref, neccesitating the use of a lambda wrapper
        :param added_functionality: Function pointer referencing a function handling the
        addition of specific extra functionality added to a method neccesitating the use
        of a lambda
        :param method: Name of arbitrary functionality or extra method to be added to a lambda
        that is not present in c++ side but required by python/pybind11
        """
        constructor_str = ""
        body_str = "{return_} {method}({args});"
        return_str = ""
        if member_function.return_type:
            return_str = "return"
        method = member_function.name
        if module:
            method = "self." + method
            constructor_str += member_function.parent.decl_string + " &self,"

        body_arg_str = ""
        # TODO: improve how we determine matrix types
        for arg in member_function.arguments:
            if added_functionality:
                spc_constructor, spc_body_arg = added_functionality(arg)
                if spc_constructor and spc_body_arg:
                    constructor_str += spc_constructor
                    body_arg_str += spc_body_arg
                    continue

            constructor_str += (
                " " + arg.decl_type.decl_string + " " + arg.name + ","
            )
            body_arg_str += arg.name + ", "
        body_arg_str = body_arg_str.strip(", ")
        constructor_str = constructor_str.strip(", ")

        body_str = body_str.format(
            return_=return_str, method=method, args=body_arg_str
        )
        if body_method:
            body_str = body_method + "\n" + body_str
        return self.opts.generic_lambda_def_fmt.format(
            module="" if module else "m",
            name=py_name if py_name else member_function.name,
            state="",
            sig=constructor_str,
            body=body_str,
            ending="" if module else ";",
            return_policy=return_value_policy,
        )

    def check_in_attributes(self, data, attr_string):
        # Check current classes for "final" attributes
        if data.attributes:
            return attr_string in data.attributes
        return False

    def generate_function_string(
        self,
        member_function,
        is_free_function=False,
        module_name="m",
        py_name="",
        publicist_name="",
        keep_alive="",
        return_value_policy="",
        pass_by_ref=False,
    ):
        """
        Takes a set of PyGCCXML data and returns the PyBind11 code for
        that function

        :param member_function: PyGCCXML data dictionary for individual function
        :param is_free_function: Boolean to determine if function should be marked
        as a member of the PyBind11 module or of a class. Essentially, prepends "m" to function signature
        is_free_function = True
          m.def("ExtractDoubleOrThrow", py::overload_cast<double const &>(ExtractDoubleOrThrow),<...>
        is_free_function = False
          .def("start_time", py::overload_cast<>(start_time),, , doc.PiecewiseTrajectory.start_time.doc)
        :param py_name: Name of the function on the python side. Defaults to the same
        name as the C++ function
        :param module_name: Name of the pybind11 module to apply free function to, default to "m". Will be set
        as submodules are created.
        :return: A string which contains all PyBind11 declarations for single function
        """
        # If a different name is requested on the Python side, set it here
        fun_name = py_name if py_name else member_function.name

        # Capture each argument and a default value, if found.
        arg_string = ""
        signature = ""

        if self.uses_shared_stl(member_function):
            signature = self.lambda_abstract_shared_stl(
                member_function, member=not is_free_function
            )

        elif self.check_unique_ptr(member_function):
            signature = self.lambda_abstract_unique_member(
                member_function, member=not is_free_function
            )
        elif self.has_ref_args(member_function) and pass_by_ref:
            arg_string = ""
            signature = self.enable_pbr_immutable(
                member_function, member=not is_free_function
            )

        else:
            function_args = member_function.arguments
            # New to pygccxml,  If function is overriding a parent function,
            # use the argument list from the overridden function to properly
            # capture the names of the arguments
            if member_function.overrides:
                function_args = member_function.overrides.arguments
            for arg in function_args:

                default_val = ""
                if arg.default_value:
                    fmt_string = self.opts.arg_val_cast_fmt
                    if (
                        " " in arg.decl_type.decl_string
                        and not arg.default_value.startswith("{")
                    ):
                        fmt_string = self.opts.nullptr_arg_val_fmt
                    default_val = fmt_string.format(
                        type=arg.decl_type.decl_string.replace(
                            "const &", ""
                        ).strip(),
                        val=arg.default_value,
                    )
                next_arg_str = self.opts.member_func_arg_fmt.format(
                    arg.name, default_val
                )
                arg_string = arg_string + ", " + next_arg_str

            if member_function.parent.name == "::":
                ref_string = "&%s" % (member_function.name)

            else:
                parent = ""
                if not is_free_function and self.protected_filter(
                    member_function
                ):
                    if publicist_name:
                        parent = publicist_name
                    else:
                        msg = (
                            "No publicist name set for protected virtual method %s"
                            % member_function.name
                        )
                        raise RuntimeError(msg)
                else:
                    parent = member_function.parent.decl_string
                ref_string = "&%s::%s" % (parent, member_function.name)
            # Check to see if a return class might be protected.
            decl_string = member_function.decl_string
            if self.check_protected_return_class(member_function):
                # Replace the first instance of the parent's declstring
                # It should replace the return type only.
                decl_string = decl_string.replace(
                    member_function.parent.decl_string,
                    self.template_args_to_underscores(
                        member_function.parent.name
                    )
                    + "_publicist",
                    1,
                )
            signature = self.opts.overload_template_fmt.format(
                fun_ref=ref_string, decl_string=decl_string
            )

        # Check to see if function can be marked as static
        static = ""
        if "has_static" in dir(member_function):
            static = "_static" if member_function.has_static else ""

        # Return formatted function string
        member_string = self.opts.member_func_fmt.format(
            module=module_name if is_free_function else "",
            static=static,
            fun_name=fun_name,
            fun_ref=signature,
            args=arg_string,
            classname_doc=member_function.parent.decl_string,
            doc=', \nR"""(%s)"""' % " \n".join(member_function.comment.text)
            if member_function.comment.text and self.opts.eds_flag
            else "",
            ending=";" if is_free_function else "",
            keepalive=keep_alive,
            return_policy=return_value_policy,
        )
        member_string.strip(",")

        return member_string

    def find_getter(self, var_data, class_data):
        name_to_find = "get_" + var_data.name
        if name_to_find in [fxn.name for fxn in class_data.member_functions()]:
            return name_to_find

        return ""

    def find_setter(self, var_data, class_data):
        name_to_find = "set_" + var_data.name
        if name_to_find in [fxn.name for fxn in class_data.member_functions()]:
            return name_to_find

        return ""

    def generate_generic_lambda(self, component):
        arg_str = ""
        state = ""
        body_str = ""
        for arg in component.arguments():
            arg_str += arg.decl_type.decl_string + " ,"

    def generate_operator_string(
        self, instance_data, oper_data, is_member_fxn=True, is_stream=False
    ):
        """
        Accepts a pygccxml data object which represents a marked operator for a
        class.  Check for overloads and generate a valid binding code as a string to
        be returned
        :param oper_data: A pygccxml object representing a variable
        :return: a string containing the PyBind11 declaration for the operator
        """
        if oper_data.name == "operator=":
            return ""

        num_args = len(oper_data.arguments)
        symbol = oper_data.symbol
        py_name = ""
        oper_arg_string = ""
        fn_call = []
        is_unary = False
        # Some C++ operators have multiple python names depending on
        # the number of arguments. We'll check which py_name to use here
        for arg in oper_data.arguments:
            oper_arg_string += arg.decl_type.decl_string + " " + arg.name + ","
            fn_call.append(arg.name)
        if symbol in arg_dependent_ops:
            if symbol in ["<<"]:
                py_name = names_dict[symbol][0]
                if is_stream:
                    single_arg = (
                        oper_data.arguments[1].decl_type.decl_string
                        + " "
                        + oper_data.arguments[1].name
                    )
                    return self.opts.repr_function_fmt.format(
                        arg_type=single_arg,
                        stream_type="o",
                        symbol=symbol,
                        arg_name=oper_data.arguments[1].name,
                    )
                else:
                    print(
                        "'operator>>' for class, {}, cannot be wrapped.  No data will be added for that operator".format(
                            instance_data.name
                        )
                    )
                    return ""
            else:
                is_unary = (
                    is_member_fxn
                    and num_args == 0
                    or not is_member_fxn
                    and num_args == 1
                )

                if is_unary:
                    py_name = names_dict[symbol][0]

                else:
                    py_name = names_dict[symbol][1]

        else:
            try:
                py_name = names_dict[symbol]
            except KeyError:
                print(
                    "Warning unknown operator %s will not be bound"
                    % oper_data.symbol
                )
                return ""

        if not py_name:
            raise RuntimeError("py_name not set")
        if instance_data != oper_data.parent:
            # Parent is not the desired class, likely a friend function. Try to use a new format
            return self.opts.call_operator_fmt.format(
                py_name=py_name,
                arg_str=oper_arg_string.rstrip(","),
                fn_call=symbol.join(fn_call),
            )
        return self.generate_function_string(oper_data, py_name=py_name)

    def generate_member_var_string(self, var_data, written_functions):
        """
        Accepts a pygccxml object representing a variable.
        Checks whether the variable is writeable and static,
        then generates valid binding code for that variable as a string.

        :param var_data: a PyGCCXML object representing a variable
        :param written_functions: Growing list of bound functions generated
        This is necessary since, if pm_flag is true, we will start binding getters and setters here.
        We don't want to duplicate them when we bind the member functions, so we'll mark them here
        :return: a string containing the PyBind11 declaration for the member variable
        """

        var_name_str = var_data.name
        classname_str = var_data.parent.decl_string

        is_public = var_data.access_type == dec.ACCESS_TYPES.PUBLIC

        if is_public:
            # Check if the variable is writeable
            is_const = dec.is_const(
                dec.remove_volatile(dec.remove_reference(var_data.decl_type))
            )
            writeable_str = "write" if not is_const else "only"

            # Get a string representing a reference to the variable
            ref = self.opts.member_reference_fmt.format(
                classname=classname_str, member=var_name_str
            )

            # Check if it is static
            is_static = var_data.type_qualifiers.has_static
            static_str = "_static" if is_static else ""

            return self.opts.public_member_var_fmt.format(
                write=writeable_str,
                static=static_str,
                var_name=var_name_str,
                var_ref=ref,
            )
        elif self.opts.pm_flag:
            # First find if there is a corresponding getter/setter
            getter_fxn_name = self.find_getter(var_data, var_data.parent)
            setter_fxn_name = self.find_setter(var_data, var_data.parent)

            # If neither could be found, return empty str
            if not getter_fxn_name and not setter_fxn_name:
                return ""

            # At this point, at least one of the functions was found,
            # so we can start generating the string
            accessors_string = ""
            is_readonly = True

            # Add the getters and setters
            if getter_fxn_name:
                ref = self.opts.member_reference_fmt.format(
                    classname=classname_str, member=getter_fxn_name
                )
                accessors_string += ref
                written_functions.append(getter_fxn_name)

            if setter_fxn_name:
                ref = self.opts.member_reference_fmt.format(
                    classname=classname_str, member=setter_fxn_name
                )
                separator = ", " if accessors_string else ""
                accessors_string += separator + ref
                written_functions.append(setter_fxn_name)

                # Also change the status of is_readonly
                is_readonly = False

            readonly_str = "_readonly" if is_readonly else ""

            is_static = var_data.type_qualifiers.has_static
            static_str = "_static" if is_static else ""

            return self.opts.private_member_var_fmt.format(
                readonly=readonly_str,
                static=static_str,
                var_name=var_name_str,
                var_accessors=accessors_string,
            )

        # If the variable isn't public, and the option to
        # expose the variable through getters and setters is False,
        # the variable won't be directly accessible from python
        # Getters and setters may still be bound when the member fxns are written
        return ""

    # Takes a list of function data, turns it into a long string of data
    # writes that string to a named file.
    # TODO: Documentation
    def write_non_class_data(
        self,
        module_name,
        function_data,
        enum_data,
        out_dir,
        found_includes,
        fun_yaml={},
        enum_yaml={},
        is_custom=False,
    ):
        """
        Takes a list of function objects and writes them out to a PyBind11 module.
        The module is named for the first argument and the file is written to the out_dir


        :param module_name: String name of grouping to be used as PyBind11 module name
        :param function_data: List of PyGCCXML objects which describe functions
        :param out_dir: File location to store the resultant file.
        :param found_includes:
        :param fun_yaml: Yaml information parsed from wrapper_input about given free functions
        :param enum_yaml: Yaml information parsed from wrapper_input about given enums
        :param is_custom: A boolean value to describe if the namespace has been added via customization
        :return: None
        """

        self.indent += " " * 2
        keys = {
            "includes": self.get_include_str(sorted(found_includes)),
            "namespace": self.most_recent_namespace(module_name) + "_py",
            "defs": "",
            "forwards": "",
            "module": self.opts.module_name,
            "name": module_name,
            "namespace_str": "",
        }
        free_fun_mod_name = "{}_free_functions".format(self.opts.module_name)
        cpp_body = self.opts.non_class_module_cpp_fmt
        pybind_module_name = "m"
        if module_name != free_fun_mod_name:
            pybind_module_name = "Py" + self.most_recent_namespace(module_name)
            keys["mod_name"] = pybind_module_name
            if len(found_includes) and not is_custom:
                keys["namespace_str"] = "using namespace %s;\n" % module_name
        keep_alive_dict = self.customizer.get_keepalive(fun_yaml)
        for function in sorted(
            function_data, key=lambda c: self.get_sort_key(c)
        ):
            keep_alive_str = self.generate_keep_alive_string(
                function, keep_alive_dict
            )
            cpp_name = function.name
            cpp_fun = fun_yaml[cpp_name]
            pbr = self.customizer.get_pass_by_ref(cpp_fun)
            cust_name = self.customizer.get_custom_name(cpp_fun)
            eigen_lambda_string, rvp = self.format_eigen_ref(function)
            keys["defs"] += (
                eigen_lambda_string
                if eigen_lambda_string
                else self.generate_function_string(
                    function,
                    py_name="" if cust_name is None else cust_name,
                    module_name=pybind_module_name,
                    is_free_function=True,
                    return_value_policy=rvp,
                    pass_by_ref=pbr,
                )
            )
            keys["defs"] += self.indent

        # TODO: Need to find other enum types to use as examples.
        for declaration in sorted(
            enum_data, key=lambda c: self.get_sort_key(c)
        ):
            cpp_name = declaration.name
            cpp_enum = enum_yaml[cpp_name]
            if declaration.decl_string == "::":
                continue
            keys["defs"] += self.generate_enum_string(
                pybind_module_name, declaration, cpp_enum
            )
        keys["defs"] = keys["defs"].strip()

        file_name = posixpath.join(
            out_dir, "%s_py.cpp" % self.most_recent_namespace(module_name)
        )
        if module_name != free_fun_mod_name:
            cpp_body = self.opts.non_class_module_return_cpp_fmt
            submodules_str = (
                self.opts.submodule_signature_fmt.format(
                    mod_name=pybind_module_name,
                    name=self.most_recent_namespace(module_name),
                )
                + "\n"
                + self.indent
            )

            keys["defs"] = submodules_str + keys["defs"]
        if module_name != free_fun_mod_name:
            keys["defs"] = keys["defs"].replace("::%s::" % module_name, "")

        content = cpp_body.format(**keys)
        if self.opts.exdec:
            content = self.normalize(content)

        self.write_data_to_file(file_name, content)
        self.indent = self.indent[:-2]

    def return_policy_reference_internal(self):
        return self.opts.return_policy_fmt.format(policy="reference_internal")

    def extract_templ_indicies_rec(self, idx, search_str):
        while search_str[idx] != ">":
            if search_str[idx] == "<":
                idx = self.extract_templ_indicies_rec(idx + 1, search_str)
            idx += 1
        return idx

    # TODO: Add recursion to handle further template nesting
    def extract_type_from_str(self, in_str, dep_set):
        itr1, itr2 = 0, 0
        while itr2 != ">":
            if in_str[itr2] == ",":
                dep_set.add(in_str[itr1:itr2])
                itr2 += 1
                itr1 = itr2
            elif in_str[itr2] == "<":
                while in_str[itr2] != ">":
                    itr2 += 1
                itr2 += 1
                dep_set.add(in_str[itr1:itr2])
            itr2 += 1

    def is_templated(self, x):
        if re.search("<.+?>", x):
            return True
        return False

    def fetch_templated_class_types(self, class_inst):
        deps_list = dec.templates.split(class_inst.decl_string)
        deps_list = deps_list[1]
        deps = list(map(lambda x: x.strip(" "), deps_list))
        return deps

    def is_primative(self, x):
        return (
            x in pygccxml.declarations.cpptypes.FUNDAMENTAL_TYPES
            or x == "long"
        )

    # TODO: Determine if templated type is a value like 'foo' or 1
    def is_value(self, x):
        pass

    # TODO improve for enums and typedefs
    def is_defined_object(self, x):
        defs = self.name_data.classes(
            lambda inst: re.search(x, inst.decl_string), recursive=True
        )
        for def_ in defs:
            if def_.decl_string is not x:
                return True
        return False

    def check_templ_includes(self, dep):
        if not (self.is_primative(dep) or self.is_defined_object(dep)):
            return False
        return True

    def search_enum_values(self, dep, value):
        for enum_val in dep.values:
            if value in enum_val:
                return True
        return False

    def get_templ_dep_includes(self, dep):
        extra_includes = set()
        # We know type is non-primative
        if not self.is_primative(dep) and not self.check_decl("std", dep):

            # need .h file if we do not already include it

            # First check if dependency is actually present in current
            # CU context - this can be handeled by the calling function
            # for now, but eventually may need to recursively search headers

            # Then get requisite #include, by querying location attribute
            # if non templated class
            # dep is string value, find associated class_t object
            qualified_dep = self.add_namespace(" ", dep).strip(" ")
            found_dep = self.name_data.classes(
                lambda x: qualified_dep == x.decl_string, recursive=True
            )
            # class_t inst dep not found, search instead for enums
            # TODO add typedef search support
            if not found_dep.declarations:
                found_dep = self.name_data.enumerations(
                    lambda x: self.search_enum_values(x, dep)
                )
            if len(found_dep):
                found_dep = found_dep[0]
                if self.is_templated(dep):
                    extra_includes.add(
                        found_dep.constructors()[0].location.file_name
                    )
                else:
                    extra_includes.add(found_dep.location.file_name)
                # and by querying constructor location if templ
        return extra_includes

    def is_stl(self, x):
        return self.check_decl("std", x.decl_string)

    def depends_on_eigen(self, x, member=None):
        return self.check_decl("Eigen", x.decl_string)

    def is_eigen_matrix(self, x):
        if self.depends_on_eigen(x):
            return self.check_decl("Matrix", x.decl_string)
        return False

    def eigen_is_not_ref(self, x):
        if self.depends_on_eigen(x):
            return not self.check_decl("Ref", x.decl_string)
        return True

    def check_type(self, decl, *conds, valid=True):
        valid_type = valid
        for cond in conds:
            if not cond(decl):
                valid_type = not valid
        return valid_type

    def supports_eigen_ref(self, decl):
        base_conds = [self.eigen_is_not_ref, self.is_eigen_matrix]
        support_conds = [
            (dec.type_traits.is_reference, dec.type_traits.is_const),
            (dec.type_traits.is_reference, dec.type_traits.is_pointer),
        ]
        if self.check_type(decl, *base_conds):
            if self.check_type(decl, *support_conds[0]):
                return True
            elif self.check_type(decl, *support_conds[1], valid=False):
                return True
        return False

    def check_decl(self, check, decl_string):
        return check in decl_string

    def derive_base_from_compound_type(self, decl):
        try:
            return self.derive_base_from_compound_type(decl.base)
        except AttributeError:
            return decl

    def include_eigen(self, x, includes):
        if not self.filter_component_deps(x, self.depends_on_eigen, True):
            eigen_str = "#include <pybind11/eigen.h>\n"
            if not re.search(eigen_str, includes):
                includes += eigen_str
        return includes

    def returns_eigen(self, member_method):
        return self.check_decl("Eigen", member_method.return_type.decl_string)

    def has_eigen_args(self, member_method):
        for arg in member_method.arguments:
            arg_type = self.derive_base_from_compound_type(arg.decl_type)
            derived_type = dec.type_traits.remove_declarated(arg_type)
            if self.depends_on_eigen(derived_type):
                return True
        return False

    def add_eigen_ref_holder(self, method_decl, const_=False, ref_=False):

        matrix_type = "{decl}".format(decl=method_decl,)
        holder = self.opts.matrixDRef.format(
            matrix_type=matrix_type, ref="&" if ref_ else "",
        )
        return holder

    def set_eigen_return_policy(self, member_method):
        # If return Eigen object is an lvalue ref or ptr
        # or is an Eigen map like type, we need to prevent
        # default pybind11 behavior from generating an unneccesary
        # and costly copy - we do this with proper management
        # of pybind return value policy
        # TODO: Once keep alive policy is merged into master
        # determine value of using keeep alive policy with the map
        # like types
        # TODO: Eigen sparse types are always copied, check this
        # return for a sparse type to make sure we DONT apply the ref

        if (
            dec.type_traits.is_reference(member_method.return_type)
            or dec.type_traits.is_pointer(member_method.return_type)
            or ("Ref" in member_method.return_type.partial_decl_string)
        ):
            return self.return_policy_reference_internal()
        # TODO if ptr or ref to MAP like type... I dont think eigen
        # encourages that behavior, but if occurs, will need a special
        # case to handle - maybe something like clone slice prevention
        return ""

    def format_eigen_ref(self, member_method, module=""):
        # format a lambda wrapper around given member
        # if member either returns or takes an Eigen object
        # so that we can use Eigen::Ref<MatrixXd>
        eigen_str = ""
        rvp = ""
        if self.returns_eigen(member_method) and self.opts.eigen_ret_pol:
            # return noraml string with added return
            # value policy
            rvp = self.set_eigen_return_policy(member_method)
        if (
            self.has_eigen_args(member_method)
            and self.supports_eigen_ref(member_method)
            and self.opts.eigen_ref
        ):
            eigen_str += self.generate_lambda_string(
                member_method,
                module=module,
                added_functionality=self.wrap_eigen_args,
                return_value_policy=rvp,
            )

        return eigen_str, rvp

    def ensure_deps(self, x):
        return self.check_component_deps(x)

    def ensure_templ_deps(self, x, includes):
        extra_includes = set()
        if self.is_templated(x.name):
            deps = self.fetch_templated_class_types(x)
            for dep in deps:
                if self.check_templ_includes(dep):
                    if includes is not None:
                        extra_includes.update(self.get_templ_dep_includes(dep))
                else:
                    return False
        if extra_includes:
            includes.update(extra_includes)
        return True

    def filter_bound(self, x):
        bound_classes = self.data_web.keys()
        if x.name not in bound_classes:

            # Base class not found in this module's scope
            # do not bind
            # TODO Raise warning that bindings
            # will not include this inheritance
            # relationship

            return False
        return True

    def get_desired_type_traits(self, decl_type, type_check):
        types = set(
            map(lambda x: type(x), dec.type_traits.decompose_type(decl_type))
        )
        return type_check.intersection(types)

    def load_validation_filters(self):
        self.validation_filters.append(self.check_component_deps)
        self.validation_filters.append(self.static_const_filter)

    def filter_component_deps(self, x, filter, boolean=False):
        for dep_info in pygccxml.declarations.get_dependencies_from_decl(x):
            for dep in dep_info.find_out_depend_on_it_declarations():
                if bool(filter(dep, x)) is boolean:
                    return False
        return True

    def dep_filter(self, x, member):
        try:
            search_data = self.name_data
            for (
                name_str
            ) in pygccxml.declarations.declaration_utils.declaration_path(x):
                # Assume anthing under std will exist and don't descend further
                if name_str == "std":
                    return True
                # Accounts for the first level where name is likely "::"
                if name_str == search_data.name:
                    continue
                # Else descend into the namespace to avoid recursive searching of
                # all name data
                if search_data.namespaces(name_str):
                    search_data = search_data.namespace(name_str)
                    continue
                # No more namespaces, lets look!
                decl = search_data.decl(name=x.name)

        # Declaration not found... c++ base should not compile, sanity check
        except pygccxml.declarations.runtime_errors.declaration_not_found_t:
            print(
                "Warning member {} will not be bound due to unmet dependency {}".format(
                    member.name, x.name
                )
            )
            return False

        # multiple declarations indicate the presense of constructors and a definition
        # meaning dependency definition is present and no compile time failures
        # however no garuntees that this type is bound within this module or others
        # TODO: Check if we want potentially non-pybind11 types bound
        except pygccxml.declarations.runtime_errors.multiple_declarations_found_t:
            return True

        # if neither exception is thrown check to see if dep is eigen or stl, if not
        # we have a declaration but no definition in current compilation context
        # passing this type to pybind11 will cause compile failure
        # if it does not cause compile failure, it still may not be a known type
        # so importing this module into python will fail
        else:
            if (
                self.depends_on_eigen(decl)
                or self.is_stl(decl)
                or type(decl) is dec.class_declaration.class_t
                or type(decl) is dec.enumeration.enumeration_t
            ):
                return True
            print(
                "Warning member {} will not be bound due to unmet dependency {}".format(
                    member.name, x.name
                )
            )
            return False

    def check_unique_ptr(self, decl):
        return self.check_ptr_type(decl, "unique_ptr")

    def check_shared_ptr(self, decl):
        return self.check_ptr_type(decl, "shared_ptr")

    def check_ptr_type(self, decl, ptr_type):
        for arg in decl.arguments:
            if self.check_decl(
                ptr_type, dec.type_traits.base_type(arg.decl_type).decl_string
            ):
                return True
        return False

    def uses_smart_ptr(self, decl):
        return self.check_shared_ptr(decl) and self.check_unique_ptr(decl)

    def lambda_abstract_unique_member(self, decl, member=False):
        lambda_signature = ""
        lambda_return = ""
        lambda_body = ""
        name = ""
        type_ = ""
        for arg in decl.arguments:
            name = arg.name
            base_decl_string = dec.type_traits.base_type(
                arg.decl_type
            ).decl_string
            if dec.templates.is_instantiation(base_decl_string):
                dec_type = dec.templates.split(base_decl_string)
                if "unique_ptr" in dec_type[0] and self.opts.cxx14_flag:
                    type_ = dec_type[1][0]
                    lambda_return += (
                        self.opts.make_unique_fmt.format(type=type_, var=name)
                        + ","
                    )
                elif "unique_ptr" in dec_type[0] and not self.opts.cxx14_flag:
                    type_ = dec_type[1][0]
                    lambda_return += (
                        self.opts.compose_unique.format(type=type_, var=name)
                        + ","
                    )
                else:
                    type_ = arg.decl_type.decl_string
                    lambda_return += name + ", "
            else:
                type_ = arg.decl_type.decl_string
                lambda_return += name + ", "
            lambda_signature += type_ + " " + name + ", "

        call = "{}({});"
        lambda_return = lambda_return.strip(", ")
        if member:
            lambda_signature = (
                "{} &self, ".format(decl.parent.decl_string) + lambda_signature
            )
            call = call.format("self." + decl.name, lambda_return)
        else:
            decl_string = ""
            if decl.parent.name == decl.name:
                decl_string = "new " + decl.parent.decl_string
            else:
                decl_string = decl.parent.decl_string
                decl_string += (
                    "::" + decl.name if decl_string != "::" else decl.name
                )

            call = call.format(decl_string, lambda_return)
        lambda_signature = lambda_signature.strip(", ")

        if type(decl.return_type) is not dec.cpptypes.void_t:
            call_ret = "return "
            if decl.return_type:
                ret_base_decl_type_t = dec.type_traits.base_type(
                    decl.return_type
                )
                if dec.templates.is_instantiation(
                    ret_base_decl_type_t.decl_string
                ):
                    ret_base_decl_templ_type = dec.templates.split(
                        ret_base_decl_type_t.decl_string
                    )
                    if (
                        "unique_ptr" in ret_base_decl_templ_type[0]
                        and ret_base_decl_templ_type[1][0]
                        in dec.cpptypes.FUNDAMENTAL_TYPES
                    ):
                        call = "*" + call
            call = call_ret + call

        return self.opts.generic_lambda_fmt.format(
            state="",
            signature=lambda_signature,
            body=lambda_body,
            return_=call,
        )

    def lambda_abstract_shared_stl(self, decl, member=False):
        lambda_signature = ""
        lambda_return = ""
        lambda_body = ""
        name = ""
        type_ = ""
        for arg in decl.arguments:
            name = arg.name
            dec_type = dec.templates.split(
                dec.type_traits.base_type(arg.decl_type).decl_string
            )
            if "unique_ptr" in dec_type[0] and self.opts.use_cxx_14:
                type_ = dec_type[1][0]
                lambda_return += (
                    self.opts.make_unique_fmt.format(type=type_, var=name)
                    + ","
                )
            elif "unique_ptr" in dec_type[0] and not self.opts.use_cxx_14:
                type_ = dec_type[1][0]
                lambda_return += (
                    self.opts.compose_unique.format(type=type_, var=name) + ","
                )
            elif "shared_ptr" in dec_type[0] and (
                self.holds_container_type(dec_type[1])
                or dec_type[1][0] in dec.cpptypes.FUNDAMENTAL_TYPES
            ):
                type_ = dec_type[1][0]
                lambda_return += (
                    self.opts.make_shared_fmt.format(type=type_, var=name)
                    + ","
                )
            else:
                type_ = arg.decl_type.decl_string
                lambda_return += name + ", "
            lambda_signature += type_ + " " + name + ", "

        # TODO Call needs a reformat: need to additionally handle wrapped stl containers here

        call = "{}({});"
        lambda_return = lambda_return.strip(", ")
        if member:
            lambda_signature = (
                "{} &self, ".format(decl.parent.decl_string) + lambda_signature
            )
            call = call.format("self." + decl.name, lambda_return)
        else:
            call = call.format(
                decl.parent.decl_string + "::" + decl.name, lambda_return
            )
        lambda_signature = lambda_signature.strip(", ")

        if type(decl.return_type) is not dec.cpptypes.void_t:
            return_ = "return "
            ret_base_decl_type_t = dec.type_traits.base_type(decl.return_type)
            ret_base_decl_templ_type = dec.templates.split(
                ret_base_decl_type_t.decl_string
            )
            if (
                dec.pointer_traits.smart_pointer_traits.is_smart_pointer(
                    ret_base_decl_type_t
                )
                and (
                    self.holds_container_type(
                        dec.templates.split(ret_base_decl_type_t.decl_string)[
                            1
                        ]
                    )
                    or ret_base_decl_templ_type[1][0]
                    in dec.cpptypes.FUNDAMENTAL_TYPES
                )
            ) or (
                "unique_ptr" in ret_base_decl_templ_type[0]
                and ret_base_decl_templ_type[1][0]
                in dec.cpptypes.FUNDAMENTAL_TYPES
            ):
                return_ += "*"
            call = return_ + call

        return self.opts.generic_lambda_fmt.format(
            state="",
            signature=lambda_signature,
            body=lambda_body,
            return_=call,
        )

    def constructor_unique_lambda(self, decl):
        return self.opts.custom_ctor_fmt.format(
            cust_ctor=self.lambda_abstract_unique_member(decl)
        )

    def constructor_shared_stl_lambda(self, decl):
        return self.opts.custom_ctor_fmt.format(
            cust_ctor=self.lambda_abstract_shared_stl(decl)
        )

    def member_unique_lambda(
        self, decl, module=None,
    ):
        func = self.lambda_abstract_unique_member(decl, member=True)
        return self.opts.member_func_fmt.format(
            module=module if module else "",
            static="",
            fun_name=decl.name,
            fun_ref=func,
            args="",
            doc=', \nR"""(%s)"""' % " \n".join(decl.comment.text)
            if decl.comment.text and self.opts.eds_flag
            else "",
            ending=";" if module else "",
            keepalive="",
            return_policy="",
        )

    def get_bases(self, inst):
        base_str_list = []
        for base in inst.bases:
            base_str_list.append(base.related_class.name)

    def uses_shared_stl(self, decl):
        for arg in decl.arguments:
            base_type = dec.type_traits.base_type(arg.decl_type)
            if self.is_templated(base_type.decl_string):
                if dec.pointer_traits.smart_pointer_traits.is_smart_pointer(
                    base_type
                ):
                    components = dec.templates.split(base_type.decl_string)[1]
                    for component in components:
                        if (
                            dec.container_traits.find_container_traits(
                                component
                            )
                            or component in dec.cpptypes.FUNDAMENTAL_TYPES
                        ):
                            return True
        if decl.return_type:
            if dec.pointer_traits.smart_pointer_traits.is_smart_pointer(
                dec.type_traits.base_type(decl.return_type)
            ) and (
                self.holds_container_type(
                    dec.templates.split(
                        dec.type_traits.base_type(decl.return_type).decl_string
                    )[1]
                )
                or dec.type_traits.base_type(decl.return_type).decl_string
                in dec.cpptypes.FUNDAMENTAL_TYPES
            ):
                return True
        return False

    def holds_container_type(self, template_insts):
        for inst in template_insts:
            if dec.container_traits.find_container_traits(inst):
                return True
        return False

    # TODO: Potentially make this more robust
    # check for typedefs, recursive inheritance, etc
    def is_shared_from_this(self, base):
        if "enable_shared_from_this" in base.name:
            return True
        return False

    def has_ref_args(self, method):
        for arg in method.arguments:
            if type(arg.decl_type) is dec.cpptypes.reference_t:
                return True
        return False

    def enable_pbr_immutable(self, method, member=False):
        signature = ""
        return_vals = ""
        return_style = self.opts.make_tuple_fmt
        method_name = method.name
        body_statement = method.name + "({});"
        call_args = ""
        return_statement = "return "
        seperator = ", "

        for arg in method.arguments:
            name = arg.name
            arg_type = arg.decl_type.decl_string
            call_args += name + seperator
            if type(arg.decl_type) is dec.cpptypes.reference_t:
                return_vals += name + seperator
            signature += arg_type + " " + name + seperator
        signature = signature.strip(", ")
        body_statement = body_statement.format(call_args.strip(", "))

        if member and not method.has_static:
            self_type = method.parent.decl_string
            signature = self_type + " &self" + seperator + signature
            body_statement = "self." + body_statement
        else:
            self_qual = method.parent.decl_string
            self_qual += "::" if self_qual != "::" else ""
            body_statement = self_qual + body_statement

        ret = method.return_type
        if type(ret) is not dec.cpptypes.void_t:
            return_vals += "ret, "
            body_statement = "auto ret = " + body_statement
        return_style = return_style.format(return_vals.strip(", "))
        return_statement += return_style + ";"
        return self.opts.generic_lambda_fmt.format(
            state="",
            signature=signature,
            body=body_statement,
            return_=return_statement.strip(", "),
        )

    def check_component_deps(self, x):
        if self.filter_component_deps(x, self.dep_filter):
            return self.filter_component_deps(x, self.skip_filter)
        return False

    def skip_filter(self, x, member=None):
        if x.name in self.skip_list:
            return False
        return True

    def const_filter(self, x):
        return x.has_const

    def static_filter(self, x):
        return x.has_static

    def static_const_filter(self, x):
        return not self.static_filter(x) and not self.const_filter(x)

    def bindable_filter(self, x):
        for filter in self.validation_filters:
            if not filter(x):
                return False
        return True

    def all_virt_filter(self, x):
        return x.virtuality != "not virtual"

    def pure_virt_filter(self, x):
        return x.virtuality == "pure virtual"

    def non_pure_virt_filter(self, x):
        return x.virtuality == "virtual"

    def public_filter(self, x):
        return x.access_type == "public"

    def protected_filter(self, x):
        # Checking classes for protected, access_type
        # is further into the object.
        if "cache" in dir(x):
            return x.cache.access_type == "protected"
        return x.access_type == "protected"

    def private_filter(self, x):
        return x.access_type == "private"

    # If the method is private and non_pure virtual, we will not add it to
    # the trampoline, as doing so would result in a compiler error.
    def virt_method_supported(self, x):
        return self.all_virt_filter(x) and not (
            self.private_filter(x) and self.non_pure_virt_filter(x)
        )

    def set_to_str(self, set_in):
        ret_str = ""
        for x in set_in:
            ret_str += str(x) + "\n"
        return ret_str

    def remove_classname_from_method(self, fun):
        fun_str = str(fun)

        # First, we have to remove the [member_function] part
        # return_type nmspc1::Base::foo(args) [member_function] ->
        # return_type nmspc1::Base::foo(args)
        fun_str = fun_str.replace(" [member function]", "")

        # Next we have to remove the current class name
        # return_type nmspc1::Base::foo(args) ->
        # return_type foo(args)
        to_be_replaced = fun.parent.decl_string.strip("::") + "::" + fun.name
        return fun_str.replace(to_be_replaced, fun.name)

    def filter_same_methods(self, method, decls):
        for decl in decls:
            if dec.function_traits.is_same_function(decl, method):
                return True
        return False

    def get_tramp_overload_macro_args(self, class_inst, alias, fun):
        keys = {
            "return_type": fun.return_type,
            "parent_alias": alias,
            "cpp_fxn_name": fun.name,
            "arg_str": "",
        }

        # Go through and construct argument string
        self.indent += " " * 2
        for i, arg in enumerate(fun.arguments):
            if i:
                keys["arg_str"] += ",\n" + self.indent
            keys["arg_str"] += arg.name

        self.indent = self.indent[:-2]

        return self.opts.pybind_overload_macro_args_fmt.format(**keys).strip()

    def find_tramp_methods(self, class_inst):
        # tramp_methods holds the methods to return
        # We'll use the signatures of the functions (with the classname removed) to check
        # whether this method needs to be overriden in the trampoline. These are stored in
        # sigs_to_skip. It could be the case that a base and derived class have methods
        # with the same name, but different return/arg types or # of args.
        # This will make sure we don't miss any.
        tramp_methods = list()
        sigs_to_skip = set()

        virt_methods = [
            f for f in class_inst.member_functions(self.all_virt_filter)
        ]
        for m in virt_methods:
            if self.virt_method_supported(m) and not self.check_in_attributes(
                m, "final"
            ):
                tramp_methods.append(m)
            # Find "real" name in via the overrides.
            if m.overrides:
                sigs_to_skip.add(
                    self.remove_classname_from_method(m.overrides)
                )
            else:
                sigs_to_skip.add(self.remove_classname_from_method(m))

        # Now we'll loop through all of the bases
        # and add any missing methods that were inherited
        # TODO: Do we need to manage skipped base classes here
        # if we are not referencing the inheritance relationship in the bindings?
        for hierarchy in class_inst.recursive_bases:
            base = hierarchy.related_class
            for m in base.member_functions(self.all_virt_filter):
                stripped_sig = self.remove_classname_from_method(m)
                if (
                    stripped_sig not in sigs_to_skip
                    and not self.filter_same_methods(m, tramp_methods)
                ):
                    if self.virt_method_supported(
                        m
                    ) and not self.check_in_attributes(m, "final"):
                        tramp_methods.append(m)

                    sigs_to_skip.add(stripped_sig)

        return sorted(tramp_methods, key=lambda c: self.get_sort_key(c))

    def get_tramp_overrides(self, class_inst, alias, tramp_methods):
        overrides_acc = ""
        published_names = set()
        for m in sorted(tramp_methods, key=lambda c: self.get_sort_key(c)):
            # Use declaration as well as name to prevent operator
            # inherited methods from appearing in tramp
            # this allows proper overloading of overridden methods
            name_sig = (m.name, m.partial_decl_string)
            if name_sig in published_names:
                continue
            published_names.add(name_sig)
            keys = dict()

            keys["fxn_sig"] = self.remove_classname_from_method(m)
            keys["pure"] = "_PURE" if self.pure_virt_filter(m) else ""

            self.indent += " " * 2
            keys["macro_args"] = self.get_tramp_overload_macro_args(
                class_inst, alias, m
            )
            self.indent = self.indent[:-2]
            keys["return_type"] = m.return_type
            overrides_acc += self.opts.tramp_override_fmt.format(**keys)
            overrides_acc += "\n" * 2 + self.indent

        return overrides_acc.strip()

    def get_trampoline_string(
        self, class_inst, cpp_class_name, tramp_name, methods
    ):
        """
        Assumes that the instance has at least 1 virtual method
        """
        alias = cpp_class_name + "_alias"
        keys = dict()
        keys["tramp_name"] = tramp_name
        keys["class_decl"] = class_inst.decl_string
        keys["parent_alias"] = alias
        keys["ctor_name"] = cpp_class_name
        keys["virtual_overrides"] = self.get_tramp_overrides(
            class_inst, alias, methods
        )

        return self.opts.trampoline_def_fmt.format(**keys).strip()

    def get_publicist_using_directives(self, class_inst, methods):
        directives = ""
        for m in methods:
            keys = dict()
            keys["class_decl"] = class_inst.decl_string
            keys["fxn_name"] = m.name

            directives += self.opts.publicist_using_directives_fmt.format(
                **keys
            )
            directives += "\n" + self.indent

        return directives.strip()

    def get_publicist_string(self, class_inst, publicist_name, methods):
        keys = dict()
        keys["publicist_name"] = publicist_name
        keys["class_decl"] = class_inst.decl_string
        keys["using_directives"] = self.get_publicist_using_directives(
            class_inst, methods
        )

        return self.opts.publicist_def_fmt.format(**keys).strip()

    def sanitized_string(self, input_string):
        """
        Ensure input to eval is safe
        :param input_string: string to be check and sanitized
        :return: boolean indicating safety
        """
        # regex check for more than one lambda function (no nesting lambdas)
        lambda_list = re.findall("lambda", input_string)
        if len(lambda_list) > 1:
            return False
        double_under = re.findall("__.*__", input_string)
        if double_under:
            return False
        skiplist = re.findall("[*.()\n;/]", input_string)
        if skiplist:
            return False
        return True

    # TODO: Handle nested template types
    def get_templated_type(
        self, pyclass_name_stem, class_data, cust_name, idx
    ):
        """
        Extract templated type from class dec regex <.+?>

        :param pyclass_name_stem: default class name generated by pygccxml
        :param class_data: A yaml dict of class data read in from wrapper_input under a 'class' header
        :return: customized class name string based on cust_name in class_data
        """
        # Get custom name string - lambda or string
        if class_data and "inst" in class_data and class_data["inst"]:
            if type(cust_name) is not list:
                if not self.sanitized_string(cust_name):
                    raise RuntimeError(
                        "Unacceptable custom name: " + cust_name
                    )
                cust_name_gen = eval(cust_name, {"__builtins__": {}}, {})
                # Lambda function
                # Evaluate yaml custom name input string, either lambda function or list of custom names
                t_type = re.search("<.+?>", pyclass_name_stem).group()
                # Handle double templates, by breaking into a list
                t_type = t_type.strip("<").strip(">").split(",")
                pyclass_name = cust_name_gen(t_type)
            else:
                pyclass_name = cust_name[idx]
        else:
            pyclass_name = cust_name
        return pyclass_name

    def write_class_data(
        self,
        cpp_class_name,
        instance_list,
        out_dir,
        found_includes,
        desired_name,
        class_data,
        namespace,
    ):
        """
        Takes an instance of Class data from PyGCCXML and outputs a single file with
        PyBind11 declarations for the class. Includes constructors and functions

        :param cpp_class_name: Name of the class without any template args
        :param instance_list: A list of pygccxml data dictionaries. One dictionary per class instance
        :param out_dir: File location to store the resultant file.
        :param found_includes: List of files to include in the wrapping code
        :param desired_name: String value for a typedef-ed object to set the typedef name as the name for the module
        :param class_data: A yaml Python dict of class data read in from wrapper_input under a 'class' header
        :param namespace: String value for the current namespace, used in a using call and to shorten input objects
        :return: None
        """
        skip_write_flag = False
        if cpp_class_name in self.skip_list:
            skip_write_flag = True
        # Increase the indent
        self.indent += " " * 4
        newlines = "\n" * 2
        keys = {
            "tmplt_fn": "",
            "includes": self.get_include_str(sorted(found_includes)),
            "trampoline_str": "",
            "publicist_str": "",
            "namespace": cpp_class_name + "_py",
            "defs": "",
            "module": self.opts.module_name,
            "forwards": set(),
            "namespace_str": "",
        }
        if namespace and len(found_includes):
            keys["namespace_str"] = "using namespace %s;\n" % namespace
        file_name = posixpath.join(out_dir, cpp_class_name + "_py.cpp")
        class_skip_list = self.customizer.get_skiplisted_members(class_data)
        for x in self.skip_list:
            class_skip_list[x] = ""
        for idx, instance_data in enumerate(instance_list):
            dep_call_str = ""
            keys["defs"] += newlines
            pyclass_name = self.template_args_to_underscores(
                instance_data.name
            )

            # If module specific name was found here, use it instead of the above name.
            # This means somewhere else there is a class with the same name.
            if (
                "%s_%s" % (self.opts.module_name, instance_data.name)
                in self.data_web
            ):
                pyclass_name = "%s_%s" % (
                    self.opts.module_name,
                    cpp_class_name,
                )
            if not desired_name == "":
                pyclass_name = desired_name
            # If user defines custom class name, use instead of default, see 'else'
            cust_name = self.customizer.get_custom_name(class_data)
            if cust_name is not None:
                # Get custom name string, potentially with type notations inserted
                pyclass_name = self.get_templated_type(
                    instance_data.name, class_data, cust_name, idx
                )
            mod_name = "Py%s" % pyclass_name
            templating_comment = ""
            if instance_data.name in self.alias_data:
                pyclass_name.replace(
                    instance_data.name, self.alias_data[instance_data.name]
                )
                mod_name.replace(
                    instance_data.name, self.alias_data[instance_data.name]
                )

            if self.is_templated(instance_data.name):
                keys["tmplt_fn"] = self.opts.template_setup_fn_fmt
                templating_comment = (
                    "// Instantiation of %s" % instance_data.name
                )
                for _idx, template_param in enumerate(
                    pygccxml.declarations.templates.split(instance_data.name)[
                        1
                    ]
                ):
                    desired_param, is_enum_val = self.find_expanded_name(
                        template_param
                    )
                    alias_param = desired_param
                    if desired_param in self.alias_data:
                        alias_param = self.alias_data[desired_param]
                    elif not is_enum_val:
                        self.alias_data[desired_param] = alias_param.lstrip(
                            "::"
                        )
                    if not self.opts.exdec:
                        inst = self.substitute_templ_insts(template_param)
                        pyclass_name = self.template_args_to_underscores(
                            instance_data.name.replace(template_param, inst)
                        )
                        mod_name = "Py%s" % pyclass_name

            # Get the arguments to the py::class_<>() call
            # The first of which is the class name
            pyclass_args = instance_data.decl_string
            # Next any super classes
            # if desired name added as guard
            # Essentially, prevents typedefs from adding parent classes.
            # TODO: Find a better way to eliminate parent classes when necessary

            # TODO prevent bindings being written for classes w/o a parent
            # which... may not always be the best move
            if desired_name == "":
                for b in instance_data.bases:
                    # If the relationship is public, add to pyclass_args
                    if b.access_type == dec.ACCESS_TYPES.PUBLIC:
                        if self.is_shared_from_this(b.related_class):
                            pyclass_args += (
                                ", "
                                + self.opts.shared_holder.format(
                                    instance_data.decl_string
                                )
                            )
                        elif b.related_class.name in self.skip_list:
                            # TODO Warn that this classes bases will not be bound
                            # for inheritance
                            print(
                                """Warning: Class {} will not have inheritance relationship with class {} in binding code.\n
                                Unable to locate class definition.""".format(
                                    instance_data.name, b.related_class.name
                                )
                            )
                        else:
                            # Prevent base class from being included in binding decl
                            # if class is not present in wrapper yaml or other imported module
                            pyclass_args += ", " + b.related_class.decl_string

            cust_holder = self.customizer.get_custom_holder_type(class_data)
            if cust_holder:
                pyclass_args += ", " + cust_holder.format(
                    instance_data.decl_string
                )
            # Check class level dependencies including template inst deps
            temp_includes = set()
            dep_str = ""
            if not self.ensure_templ_deps(instance_data, temp_includes):
                skip_write_flag = True
            dep_str = self.get_include_str(temp_includes)

            # Check to see if we need to include eigen
            dep_str = self.include_eigen(instance_data, dep_str)
            if dep_str:
                keys["includes"] = keys["includes"] + dep_str

            attribute_info = self.customizer.get_attribute_dict(class_data)
            pyclass_attr_str = self.indent
            if attribute_info:
                for attribute in attribute_info:
                    pyclass_attr_str += self.opts.attribute_fmt.format(
                        name=mod_name,
                        attribute=attribute,
                        value=attribute_info[attribute],
                    )
                    pyclass_attr_str += "\n%s" % self.indent

            # determine if PBR should be used at a class level
            # TODO: when member method granularity is introduced, use that to determine
            # PBR instead of at class level
            pbr = self.customizer.get_pass_by_ref(class_data)

            tramp_methods = self.find_tramp_methods(instance_data)
            # If there exist any virtual functions abiding by certain criteria
            # (see find_tramp_methods), we'll need to write out a trampoline implementation
            tramp_name = ""
            tramp_methods = [
                x
                for x in tramp_methods
                if x.name not in class_skip_list and (self.ensure_deps(x))
            ]
            if (
                tramp_methods
                and dec.type_traits_classes.has_trivial_constructor(
                    instance_data
                )
            ):
                self.indent = self.indent[:-2]
                tramp_name = pyclass_name + "_trampoline"
                tramp_str = self.get_trampoline_string(
                    instance_data, cpp_class_name, tramp_name, tramp_methods
                )
                keys["trampoline_str"] += tramp_str
                keys["trampoline_str"] += newlines
                if keys["namespace_str"]:
                    # Regex replace for leading "::" which may or may not exist.
                    keys["trampoline_str"] = re.sub(
                        "[:]*%s::" % namespace, "", keys["trampoline_str"]
                    )
                self.indent += " " * 2

                # Also need to add to pyclass_args
                pyclass_args += ", " + tramp_name
            else:
                tramp_methods = []

            # Now we'll deal with any protected functions, as we want these to be visible
            # to python subclasses. Especially if they are virtual and appear in the
            # trampoline for overriding.
            prot_methods = [
                x
                for x in list(
                    instance_data.member_functions(self.protected_filter)
                )
                if (
                    x.name not in class_skip_list
                    or (
                        class_skip_list[x.name]
                        and class_skip_list[x.name]["num_args"]
                        != len(x.arguments)
                    )
                )
                and (self.ensure_deps(x))
            ]
            prot_classes = list(instance_data.classes(self.protected_filter))
            total_protected = prot_classes + prot_methods
            publicist_name = ""

            total_protected = sorted(
                total_protected, key=lambda c: self.get_sort_key(c)
            )
            if total_protected:
                self.indent = self.indent[:-2]
                publicist_name = pyclass_name + "_publicist"
                publicist_str = self.get_publicist_string(
                    instance_data, publicist_name, total_protected
                )
                keys["publicist_str"] += publicist_str
                keys["publicist_str"] += newlines
                if keys["namespace_str"]:
                    # Regex replace for leading "::" which may or may not exist.
                    keys["publicist_str"] = re.sub(
                        "[:]*%s::" % namespace, "", keys["publicist_str"]
                    )
                self.indent += " " * 2

            constructor_str = ""
            # List to stuff names into which will prevent re-writing
            written_functions = []
            keep_alive_dict = self.customizer.get_keepalive(class_data)
            if not instance_data.is_abstract:

                for constructorObj in sorted(
                    instance_data.constructors(self.public_filter),
                    key=lambda c: self.get_sort_key(c),
                ):
                    if self.uses_shared_stl(constructorObj):
                        constructor_str += self.constructor_shared_stl_lambda(
                            constructorObj
                        )
                    if self.check_unique_ptr(constructorObj):
                        constructor_str += self.constructor_unique_lambda(
                            constructorObj
                        )
                    else:
                        arg_string = ""
                        arg_name_string = ","
                        keep_alive_str = self.generate_keep_alive_string(
                            constructorObj, keep_alive_dict
                        )
                        for arg in constructorObj.arguments:
                            if (
                                self.supports_eigen_ref(arg.decl_type)
                                and self.opts.eigen_ref
                            ):

                                type_checks = set(
                                    (
                                        dec.cpptypes.reference_t,
                                        dec.cpptypes.const_t,
                                    )
                                )
                                traits = self.get_desired_type_traits(
                                    arg.decl_type, type_checks
                                )

                                arg_string += (
                                    self.add_eigen_ref_holder(
                                        arg.decl_type.decl_string,
                                        dec.cpptypes.const_t in traits,
                                        dec.cpptypes.reference_t in traits,
                                    )
                                    + ","
                                )
                            else:
                                arg_string += arg.decl_type.decl_string + ","

                            default_val = ""
                            if arg.default_value:
                                fmt_string = self.opts.arg_val_cast_fmt
                                # startswith is used to prevent a (<type>){val, val, val} casting string
                                # which is a non-standard explicit type conversion syntax error.
                                if (
                                    " " in arg.decl_type.decl_string
                                    and not arg.default_value.startswith("{")
                                ):
                                    fmt_string = self.opts.nullptr_arg_val_fmt
                                default_val = fmt_string.format(
                                    type=arg.decl_type.decl_string.replace(
                                        "const &", ""
                                    ).strip(),
                                    val=arg.default_value,
                                )
                            arg_name_string += self.opts.member_func_arg_fmt.format(
                                arg.name, default_val
                            )
                            arg_name_string += ","
                        arg_string = arg_string.rstrip(",")
                        arg_name_string = arg_name_string.rstrip(",")

                        # If we're using a trampoline, we need to be careful of
                        # the copy constructor (CC).
                        if tramp_methods and dec.is_copy_constructor(
                            constructorObj
                        ):
                            cname = instance_data.decl_string
                            cc_keys = {
                                "arg_type": arg_string,
                                "cname": cname,
                                "doc": "",
                                "indent": self.indent,
                                "tramp_arg_type": arg_string.replace(
                                    cname, tramp_name
                                ),
                                "trampname": tramp_name,
                            }
                            constructor_str += self.opts.copy_constructor_tramp_fmt.format(
                                **cc_keys
                            )

                        else:
                            constructor_str += self.opts.constructor_fmt.format(
                                arg_string,
                                arg_name_string,
                                ", " if False else "",
                                keep_alive_str,
                            )

                    constructor_str += self.indent

            # If virtual functions are present and the class is abstract,
            # we actually have to utilize the default constructor for the trampoline
            elif (
                tramp_methods
                and dec.type_traits_classes.has_trivial_constructor(
                    instance_data
                )
            ):
                # No need to look for arguments
                constructor_str += self.opts.constructor_fmt.format(
                    "", "", ", " if False else "", ""
                )

            member_var_string = ""
            for member_var in sorted(
                [x for x in instance_data.variables()],
                key=lambda c: self.get_sort_key(c),
            ):
                if member_var.name in class_skip_list or not (
                    self.ensure_deps(member_var)
                ):
                    continue
                member_var_string += self.generate_member_var_string(
                    member_var, written_functions
                )
                member_var_string += self.indent

            member_string = ""
            for member_function in sorted(
                [x for x in instance_data.member_functions()],
                key=lambda c: self.get_sort_key(c),
            ):
                if (
                    member_function.name in written_functions
                    or self.private_filter(member_function)
                    or not (self.ensure_deps(member_function))
                ):
                    continue
                if member_function.name in class_skip_list:
                    if not class_skip_list[
                        member_function.name
                    ] or class_skip_list[member_function.name][
                        "num_args"
                    ] == len(
                        member_function.arguments
                    ):
                        continue
                keep_alive_str = self.generate_keep_alive_string(
                    member_function, keep_alive_dict
                )

                # Handle pybind11 Eigen::Ref avoid copy requirement
                # for member functions
                eigen_str, rvp = self.format_eigen_ref(
                    member_function, module=cpp_class_name
                )
                if eigen_str:
                    member_string += eigen_str

                elif self.check_unique_ptr(member_function):
                    member_string += self.member_unique_lambda(member_function)
                else:
                    keep_alive_str = self.generate_keep_alive_string(
                        member_function, keep_alive_dict
                    )
                    member_string += self.generate_function_string(
                        member_function,
                        publicist_name=publicist_name,
                        keep_alive=keep_alive_str,
                        return_value_policy=rvp,
                        pass_by_ref=pbr,
                    )
                member_string += self.indent

            # TODO: Necessary?  Determine usefulness of listing operators
            operator_string = ""
            total_operators = set(instance_data.operators())
            for free_op in instance_data.parent.free_operators():
                if (
                    instance_data in free_op.class_types
                    and instance_data.location.file_name
                    == free_op.location.file_name
                ):
                    total_operators.add(free_op)
            for operator in sorted(
                total_operators, key=lambda c: self.get_sort_key(c)
            ):
                if operator.name.strip("operator ") in class_skip_list:
                    if not class_skip_list[
                        operator.name.strip("operator ")
                    ] or class_skip_list[operator.name.strip("operator ")][
                        "num_args"
                    ] == len(
                        operator.arguments
                    ):
                        continue
                # If ostream is first argument, we assume this is a string representation
                # function.  Since ostream constructors are protected, we replace it with
                # a separate  function to return
                stream_regex_match = {}
                if operator.arguments and "base" in dir(
                    operator.arguments[0].decl_type
                ):
                    stream_regex_match = re.match(
                        "::std::(?P<stream_type>[io])stream",
                        operator.arguments[0].decl_type.base.decl_string,
                    )
                is_stream = False
                if stream_regex_match:
                    if stream_regex_match.group("stream_type") == "o":
                        is_stream = True
                        keys["includes"] += "#include <sstream>\n"
                    else:
                        print(
                            "The found operator, operator>>. will not be wrapped. Class {} ".format(
                                instance_data.name
                            )
                            + "will not have this information"
                        )
                        continue
                operator_string += self.generate_operator_string(
                    instance_data, operator, is_stream=is_stream
                )

                operator_string += self.indent
            no_delete_string = ""
            if not dec.type_traits_classes.has_public_destructor(
                instance_data
            ):
                no_delete_string = (
                    ", std::unique_ptr<%s, py::nodelete>"
                    % instance_data.decl_string
                )
            enum_string = ""
            for enum in sorted(
                [x for x in instance_data.enumerations()],
                key=lambda c: self.get_sort_key(c),
            ):
                enum_string += self.generate_enum_string(
                    mod_name, enum, class_data
                )
            py_module_local = ""
            is_mod_local = self.customizer.get_module_local_value(class_data)
            if is_mod_local:
                py_module_local = self.opts.module_local_fmt

            # Assume normal class_init call but if template_fn is used
            # Use that function for init.
            init_data = {
                "no_delete": no_delete_string,
                "pyclass_args": pyclass_args,
                "name": pyclass_name,
                "mod_name": mod_name,
                "mod_loc": py_module_local,
                "pyclass_args": pyclass_args,
                "doc": ', \nR"""(%s)"""'
                % " \n".join(instance_data.comment.text)
                if instance_data.comment.text and self.opts.eds_flag
                else "",
            }
            class_init_cmd = self.opts.class_init_call_fmt.format(**init_data)
            if keys["tmplt_fn"] and self.opts.twf_flag:
                init_data["mod_loc"] = py_module_local.lstrip(",")
                init_data["doc"] = init_data["doc"].lstrip(".")
                class_init_cmd = self.opts.class_init_wrapper_fmt.format(
                    **init_data
                )
            class_body = self.opts.class_info_body_fmt.format(
                class_cmd=class_init_cmd,
                tmplt_note=templating_comment,
                name=pyclass_name,
                mod_name=mod_name,
                pyclass_args=pyclass_args,
                attrs=pyclass_attr_str,
                doc=', \nR"""(%s)"""' % " \n".join(instance_data.comment.text)
                if instance_data.comment.text and self.opts.eds_flag
                else "",
                mod_loc=py_module_local,
                keepalive=keep_alive_dict,
                dependency_calls=dep_call_str,
                no_delete=no_delete_string,
                constructor=constructor_str.strip(),
                funcs=member_string.strip(),
                vars=member_var_string.strip(),
                opers=operator_string.strip(),
                enums=enum_string.strip(),
            )
            if keys["namespace_str"]:
                # Regex replace for leading "::" which may or may not exist.
                class_body = re.sub("[:]*%s::" % namespace, "", class_body)
            keys["defs"] += class_body
        keys["defs"] = keys["defs"].strip()
        keys["forwards"] = "".join(keys["forwards"])

        if skip_write_flag:
            # Empty the value here, so we don't miss the file, but write no data about a class
            # that is missing some wrapping information.
            print(
                "Warning: Class %s will not have wrapping generated due to missing information."
                % cpp_class_name
            )
            print("Missing data: %s" % self.skip_list)
            keys["defs"] = ""
            keys["forwards"] = ""

        # brutal fix, but not the worst
        file_content = self.opts.class_module_cpp_fmt.format(**keys)
        file_content = self.normalize(file_content)
        for indx, alias_value in enumerate(self.alias_data):
            if alias_value in file_content:
                file_content = re.sub(
                    r"(?P<strt>[,<( ]*)[:]*%s(?P<end>[,:> ]+)" % alias_value,
                    "\g<strt>%s\g<end>"
                    % self.alias_data[
                        alias_value
                    ],  # noqa \g is valid substitution
                    file_content,
                )

        self.write_data_to_file(file_name, file_content)
        self.indent = self.indent[:-4]

    def get_sort_key(self, pygccxml_data):
        return (
            pygccxml_data.name,
            os.path.relpath(pygccxml_data.location.file_name),
            pygccxml_data.location.line,
        )

    def build_namespace_structure(self, namespace_s, line_out, prev_nmspc):
        for key in namespace_s.keys():
            sub_name = key + "_py"
            line_out["forwards"].append(
                self.opts.init_fun_forward_non_void_fmt.format(
                    mod_name=key, name=sub_name, module=self.opts.module_name,
                )
            )
            # TODO change the init_fun_signature to include diff mod name (module)
            mod_var = (
                prev_nmspc
                if not prev_nmspc == self.opts.module_name
                else "model"
            )
            line_out["init_funs"].append(
                self.opts.submodule_init_fun_signature_return_fmt.format(
                    mod_name=key,
                    name=sub_name,
                    module=self.opts.module_name,
                    module_var=mod_var,
                )
            )
            self.build_namespace_structure(namespace_s[key], line_out, key)

    def write_module_data(self, module_name, results_dict, out_dir):
        """
        Writes out the "folder" level module for wrapping.
        This file follows
        :param module_name:
        :param results_dict:
        :param out_dir:
        :return: The name of the module file to include in the library
        """

        module_data = {"forwards": [], "init_funs": []}
        no_forward_list = set()
        nmspcs = set()
        # Note that this assumes self.name_tree will only be populated
        # if we are not ignoring the namespace structure
        if self.name_tree:
            self.flatten(self.name_tree, nmspcs)
            self.build_namespace_structure(
                self.name_tree[self.opts.module_name],
                module_data,
                self.opts.module_name,
            )

        # TODO this may need to be an insert
        if self.opts.top_module:
            module_data["init_funs"].append(
                self.opts.attribute_fmt.format(
                    name="model",
                    attribute="__name__",
                    value="{parent}.{name}".format(
                        parent=self.opts.top_module, name=self.opts.module_name
                    ),
                )
            )
        for future_file in results_dict["out_names"]:
            name = future_file.split(".")[0]
            # we define the namespace structure in above call to build_namespc_str
            # so we only need the classes and free funcs here
            if name.split("_py")[0] not in nmspcs:
                mod = results_dict["modules"][future_file]
                sub_mod = (
                    mod
                    if not mod == self.opts.module_name and bool(mod)
                    else "model"
                )
                module_data["forwards"].append(
                    self.opts.init_fun_forward_fmt.format(
                        name=name, module=self.opts.module_name,
                    )
                )
                module_data["init_funs"].append(
                    self.opts.submodule_init_fun_signature_fmt.format(
                        name=name,
                        module=self.opts.module_name,
                        module_var=sub_mod,
                    )
                )
        module_cpp_file = posixpath.join(out_dir, "%s.cpp" % module_name)
        module_cpp_text = self.opts.common_cpp_body_fmt.format(
            name=module_name,
            forwards="".join(
                module_data["forwards"]
                + list(results_dict["all_auto_bind_defs"])
            ),
            init_funs="".join(module_data["init_funs"]),
            autobind_calls="".join(results_dict["all_auto_bind"]),
        )
        self.write_data_to_file(module_cpp_file, module_cpp_text)
        return module_cpp_file

    def format_member_insts(
        self, module_info, member_insts, class_inst, class_name
    ):
        templ_str_ = "template {ret} {name}::{member}{member_inst}({inst});"
        templ_str = templ_str_
        for member in member_insts:
            insts = member_insts[member]
            for inst in insts:
                if member == class_name:
                    templ_str = templ_str.format(
                        ret="",
                        name=class_inst,
                        member=member,
                        member_inst="",
                        inst=inst,
                    )
                else:
                    member_inst = "<{}>".format(inst[0])
                    templ_str = templ_str.format(
                        ret=inst[-1],
                        name=class_inst,
                        member=member,
                        member_inst=member_inst,
                        inst=inst[0],
                    )
                module_info["templ_members"].add(templ_str)
                templ_str = templ_str_

    def add_member_templ_inst(
        self, module_info, class_insts, member_insts, class_name
    ):
        if class_insts:
            for templ in class_insts:
                self.format_member_insts(
                    module_info, member_insts, templ, class_name
                )
        else:
            self.format_member_insts(
                module_info, member_insts, class_name, class_name
            )

    def populate_namespace_web(self, mod_name):
        self.namespace_web[mod_name] = {
            "all_gen_fun_data": list(),
            "all_gen_enum_data": list(),
            "files_to_include": set(),
            "funs_dict": {},
            "enums_dict": {},
            "is_custom": False,
        }

    def populate_name_tree(self, data_tree, mod_name, mod_data):
        # if mod_data is supplied, attempt to update data
        if mod_name in data_tree:
            # But if the key exists, don't update it.
            # Prevents overwriting deeper namespaces found earlier in the file.
            if list(mod_data.keys())[0] not in data_tree[mod_name]:
                data_tree[mod_name].update(mod_data)
        else:
            for data_obj in data_tree:
                self.populate_name_tree(
                    data_tree[data_obj], mod_name, mod_data
                )

    # Adds the namespace to the function or class name
    # Can pass in "" as namespace
    def add_namespace(self, namespace, name):
        if namespace:
            return namespace + "::" + name
        else:
            return name

    def remove_namespace(self, name):
        idx = name.rfind("::")
        if idx > -1:
            idx += 2
            return name[idx:]
        else:
            return name

    def find_future_file_name(
        self, is_class, name, free_fun_name="", curr_nmspc=""
    ):
        ret = ""
        suf = "_py.cpp"
        if is_class:
            ret = name.split("<")[0]
        elif curr_nmspc:
            # Only use most recent namespace if nested
            ret = self.most_recent_namespace(curr_nmspc)
        else:  # free function in no namespace
            ret = free_fun_name
        ret += suf
        return ret

    def find_module_data(
        self,
        yaml_dict,
        res_dict,
        free_fun_name,
        curr_nmspc="",
        curr_file="",
        mod_tree={},
    ):
        auto_bind_func = "apb11_" + self.opts.module_name
        curr_mod = (
            self.most_recent_namespace(curr_nmspc)
            if curr_nmspc
            else self.opts.module_name
        )
        if not curr_nmspc:
            self.populate_namespace_web(curr_mod)
        # Allow customization at the "Files" level to add skips for all objects below.
        if "files" in yaml_dict.keys():
            [
                self.skip_list.add(x)
                for x in self.customizer.get_skiplisted_members(
                    yaml_dict["files"]
                )
            ]
        for key, inner_dict in yaml_dict.items():
            if key in ["classes", "functions", "enums"]:
                is_class = key == "classes"
                for name, data in inner_dict.items():
                    # Add this to our list of all names.
                    # We'll pass these to castxml to limit the
                    # number of pygccxml declaration objects we construct
                    res_dict["all_qualified_names"].add(
                        self.add_namespace(curr_nmspc, name)
                    )
                    # Add the dependent file
                    res_dict["to_include"].add(curr_file)
                    # Then write the future file
                    future_file = self.find_future_file_name(
                        is_class, name, free_fun_name, curr_nmspc
                    )
                    custom_mod_name = self.customizer.get_add_custom_namespace(
                        data,
                    )
                    if custom_mod_name:
                        # If this is the first we're hearing of a custom
                        # namespace structure and we're not enforcing the
                        # namespace structure, move the previous classes, fxns, etc.
                        # to the global namespace.
                        if not self.cns_flag and not self.opts.ens_flag:
                            self.namespace_web = dict()
                            self.populate_namespace_web(self.opts.module_name)
                            self.cns_flag = True

                        self.populate_namespace_web(custom_mod_name)
                        self.namespace_web[custom_mod_name]["is_custom"] = True
                        cust_nmspc_future_file = self.find_future_file_name(
                            is_class,
                            custom_mod_name,
                            free_fun_name,
                            custom_mod_name,
                        )
                        res_dict["out_names"][cust_nmspc_future_file] = ""
                        if is_class:
                            res_dict["out_names"][
                                future_file
                            ] = custom_mod_name
                    else:
                        ff_name = future_file
                        obj_to_nmspc = ""
                        if not self.opts.ens_flag:
                            if not is_class:
                                ff_name = free_fun_name + "_py.cpp"
                        else:
                            obj_to_nmspc = curr_mod
                        res_dict["out_names"][ff_name] = obj_to_nmspc
                    class_templ = ""
                    if data and "inst" in data and data["inst"]:
                        # Add all template objects to the data for qualified names
                        # Allowing data to be found
                        for inst_obj in data["inst"]:
                            if isinstance(inst_obj, list):
                                [
                                    res_dict["all_qualified_names"].add(x)
                                    for x in inst_obj
                                ]
                            else:
                                res_dict["all_qualified_names"].add(inst_obj)
                        # TODO. Logic of next 3 lines will need to change when
                        # support for typed enums is added
                        if key == "enums":
                            raise RuntimeError(
                                "Typed enums not currently supported"
                            )
                        key_to_inst_list = (
                            "class_insts" if is_class else "func_insts"
                        )
                        all_inst_names = self.get_all_inst_names(name, data)
                        all_inst_names = [
                            self.add_namespace(curr_nmspc, inst)
                            for inst in all_inst_names
                        ]
                        class_templ = all_inst_names
                        for inst in all_inst_names:
                            self.data_web[inst] = auto_bind_func

                        # Add all instanced names to the data for qualified names
                        # Allowing data to be found
                        [
                            res_dict["all_qualified_names"].add(x)
                            for x in all_inst_names
                        ]
                        res_dict[key_to_inst_list].extend(all_inst_names)
                    elif is_class:
                        res_dict["non_template_classes"].append(name)
                        # Use add_namespace to match templated classes
                        self.data_web[
                            self.add_namespace(curr_nmspc, name)
                        ] = auto_bind_func
                    templ_members = self.customizer.get_method_inst(data)
                    if templ_members:
                        self.add_member_templ_inst(
                            res_dict, class_templ, templ_members, name
                        )

            elif key == "files":
                for file_name in inner_dict:
                    self.find_module_data(
                        inner_dict[file_name],
                        res_dict,
                        free_fun_name,
                        curr_nmspc,
                        file_name,
                        mod_tree,
                    )
            elif key == "namespaces":
                for namespace_name in inner_dict:
                    new_nmspc = self.add_namespace(curr_nmspc, namespace_name)
                    # If cns_flag is enabled, we're writing everything to the
                    # top level namespace except for objects that define a
                    # custom namespace.
                    if not self.cns_flag and self.opts.ens_flag:
                        self.populate_namespace_web(new_nmspc)
                    if self.opts.ens_flag:
                        future_file = self.find_future_file_name(
                            False, new_nmspc, free_fun_name, new_nmspc
                        )
                        res_dict["out_names"][future_file] = curr_mod
                        self.populate_name_tree(
                            mod_tree, curr_mod, {namespace_name: {}}
                        )
                    mod_tree = self.find_module_data(
                        inner_dict[namespace_name],
                        res_dict,
                        free_fun_name,
                        new_nmspc,
                        curr_file,
                        mod_tree,
                    )
        return mod_tree

    def clean_flags(self, rsp_path):
        rsp_includes = []
        rsp_indx_files = []
        rsp_defs = ""
        c_std_flag = ""
        rsp_compile_opts = ""
        # No path specified? Return 0 flags
        if not rsp_path:
            return rsp_includes, rsp_defs, rsp_indx_files

        with open(rsp_path, "r") as fp:
            for line in fp.readlines():
                line = line.strip().replace(";", " ").split(" ")
                if line[0] == "includes:":
                    rsp_includes = line[1:]

                elif line[0] == "defines:":
                    rsp_defs = " ".join(
                        ["-D" + def_ if def_ else "" for def_ in line[1:]]
                    )

                elif line[0] == "c_std:":
                    c_std_flag = line[1]

                elif line[0] == "index_files:":
                    rsp_indx_files = line[1:]

                elif line[0] == "compile_opts:":
                    rsp_compile_opts = " ".join(line[1:])
                else:
                    err_msg = (
                        "ERROR: invalid first token in response file: %s"
                        % line[0]
                    )
                    raise RuntimeError(err_msg)
        if self.opts.clang_opts:
            rsp_compile_opts += " " + self.opts.clang_opts
        rsp_defs = c_std_flag + " " + rsp_defs + " " + rsp_compile_opts
        return rsp_includes, rsp_defs.strip(), rsp_indx_files

    def recompose_template_inst(self, name, insts):
        if name:
            templ_str = name + "<{}>"
            return templ_str.format(", ".join(insts))

    def find_expanded_name(self, object_name):
        """
        Takes a string of information and searches through the typedefs and enumeration
        values found by pygccxml.  Returns a string which mirrors the input string, but
        with the values replaced as they are found in pygccxml.

        :param object_name: A string which holds the name of a C++ object.
        :return: a tuple of information, first being a string with the expanded name. The
        second being a boolean value if the enumeration value path was used
        """
        # Early exit, not need to look over primative types
        if self.is_primative(object_name):
            return (object_name, False)
        else:
            templ_name = ""
            # base_name will be what we look for
            base_name = object_name
            old_name = base_name
            # Check to see if it really is a templated naem
            if dec.templates.is_instantiation(object_name):
                # Returns tuple ("<name>",["<template_name>"])
                templ_decomp = dec.templates.split(object_name)
                # Take list of types template is instantiated over
                base_name = templ_decomp[1]
                # if we have more than one inst type, derive all base types
                # and then recompose into name<type1,type2,...> format
                # handles arbitrary levels of template nesting
                if len(base_name):
                    expanded_name_lst = [
                        self.find_expanded_name(x)[0] for x in base_name
                    ]
                    new_string = self.recompose_template_inst(
                        templ_decomp[0], expanded_name_lst
                    )
                    if new_string in self.alias_data:
                        self.alias_data[object_name] = self.alias_data[
                            new_string
                        ]
                    return (new_string, False)
                else:
                    base_name = base_name[0]
                    old_name = base_name
                    templ_name = templ_decomp[0]

            # Again check for primative in template parameter
            # we recompose base_name into a template inst if that's
            # the case
            if self.is_primative(base_name):
                return (object_name, False)
                # base_name if not templ_name else self.recompose_template_inst(templ_name, [base_name])
            # Recursively look over all decls
            try:
                data = self.name_data.decl(
                    self.remove_namespace(base_name),
                    decl_type=pygccxml.declarations.typedef_t,
                    recursive=True,
                )
            # Catch anything, assuming it's not found
            except (
                pygccxml.declarations.runtime_errors.multiple_declarations_found_t,
                pygccxml.declarations.runtime_errors.declaration_not_found_t,
            ):
                found_enum = False
                # Just in case, check for enumeration values (enum_deps test)
                data = self.name_data.enumerations(
                    lambda x: self.search_enum_values(x, base_name)
                )
                if data:
                    found_enum = True
                    base_name = "%s::%s" % (data[0].name, base_name)

                # again here we recompose into a templated
                # type if relevant
                return (object_name.replace(old_name, base_name), found_enum)
                # base_name if not templ_name else self.recompose_template_inst(templ_name, [base_name])

            # Now we have data, find if it's a typedef or not
            # and remove leading "::"

            rtn = data.decl_type.declaration.decl_string.lstrip("::")
            # Remove leading  "::" again
            if object_name.startswith("::"):
                object_name = object_name.lstrip("::")
            # Send back the string with the name replaced.
            new_name = object_name.replace(base_name, rtn)
            self.alias_data[rtn] = base_name.lstrip("::")
            return (new_name, False)

    def get_all_inst_names(self, name, data):
        ret = []
        # Any template types to take care of?
        if data and "inst" in data and data["inst"]:
            # `not self.name_data` checks whether we've run
            # pygccxml or not.  find_expanded_name will only be useful if it has
            # populated self.name_data
            if not self.name_data:
                for template_param in data["inst"]:
                    template_arg_str = template_param
                    # If we have > 1 template parameter (ie, class<float, double>),
                    # we'll join them here
                    if isinstance(template_param, list):
                        template_arg_str = ", ".join(
                            map(self.remove_namespace, template_param)
                        )
                    ret.append(name + "<" + template_arg_str + ">")
            else:
                for template_param in data["inst"]:
                    if isinstance(template_param, list):
                        template_arg_str = ", ".join(
                            [
                                self.find_expanded_name(x)[0]
                                for x in template_param
                            ]
                        )
                    else:
                        template_arg_str = self.find_expanded_name(
                            template_param
                        )[0]
                    ret.append(name + "<" + template_arg_str + ">")

        else:
            ret = [name]

        return ret

    # Takes a compound namespace like foo::bar and
    # returns the most recent namespace (bar)
    def most_recent_namespace(self, nmspc):
        return nmspc.split("::")[-1]

    def find_typedef_class_data(self, typedef_list):
        # Gives us the class that the typedef is trying to be
        for typedef_object in typedef_list:
            typedef_class_string = typedef_object.decl_type.decl_string
            template_params = ""

            # Check to see if any template parameters exist
            template_params_start = typedef_class_string.find("<")

            # If so, split off the template parameters to prevent a "::" split from breaking apart
            # the arguments inside the template
            if template_params_start:
                typedef_class_string = typedef_object.decl_type.decl_string[
                    :template_params_start
                ]
                template_params = typedef_object.decl_type.decl_string[
                    template_params_start:
                ]
            typedef_class_list = typedef_class_string.split("::")

            # Reassemble class name
            class_name = typedef_class_list.pop() + template_params
            data = self.name_data

            # For each namespace found in list, proceed down through the data
            for namespace in typedef_class_list[1:]:
                data = data.namespace(namespace)
            # See if class name exists before returning it.
            instance_data = data.class_(class_name)
            if instance_data:
                return_list = []
                for b in instance_data.bases:
                    # If the relationship is public, add to pyclass_args
                    if b.access_type == dec.ACCESS_TYPES.PUBLIC:
                        return_list.append(b.related_class)
                return_list.append(instance_data)
                return return_list

    def is_in_pygccxml_list(self, in_class, out_list):
        class_names_list = [in_class.name]
        try:
            if dec.templates.is_instantiation(in_class.name):
                in_class_data = dec.templates.split(in_class.name)
                class_names_list.append(
                    self.find_expanded_name(in_class.name)[0]
                )
                # class_names_list.append("%s<%s>" % (in_class_data[0],  self.find_expanded_name(in_class_data[1][0])))
        except pygccxml.declarations.runtime_errors.multiple_declarations_found_t:
            pass
        for name in class_names_list:
            if name in out_list:
                return in_class
        return ""

    def generate_bindings(
        self,
        yaml_dict,
        pygccxml_data,
        free_fun_name="",
        file_name="",
        curr_nmspc="",
        func_dict={},
        enum_dict={},
    ):
        if "classes" in yaml_dict:
            classes_dict = yaml_dict["classes"]
            # Take this one class at a time
            class_out = {}
            include_list = set({file_name})
            for class_name, class_data in classes_dict.items():

                desired_name = ""
                names_to_find = self.get_all_inst_names(class_name, class_data)
                gen_data_for_class = pygccxml_data.classes(
                    lambda c: self.is_in_pygccxml_list(c, names_to_find)
                )
                # Desired name added to allow typedef objects to have written name instead of
                # the name of the original class.
                # TODO: typedef class check goes here.

                # Check for proper amounts of data
                # Classes with multiple instances may silently wrap partial data if
                # one of the instances isn't found by classes call
                # TODO: Determine if this should be an error
                if len(gen_data_for_class) != len(names_to_find):
                    print(
                        "Warning: All instances of %s were unable to be found"
                        % class_name
                    )
                    print(
                        "There were %s names to find but %s class(es) were returned"
                        % (len(names_to_find), len(gen_data_for_class))
                    )

                if not gen_data_for_class:
                    # Before binding, see if the class is bound elsewhere:
                    typedef = self.name_data.typedefs(
                        lambda c: c.name in names_to_find, recursive=True
                    )
                    if typedef:
                        desired_name = typedef[0].name
                        gen_data_for_class = self.find_typedef_class_data(
                            typedef
                        )
                        # But only generate data for the desired object.
                        gen_data_for_class = [gen_data_for_class[-1]]
                for class_inst in gen_data_for_class:
                    for typedef in class_inst.typedefs():
                        if "declaration" in dir(typedef.decl_type):
                            self.alias_data[
                                typedef.decl_type.declaration.decl_string
                            ] = typedef.decl_string.lstrip("::")
                class_out[class_name] = {
                    "gen_data": gen_data_for_class,
                    "desired_name": desired_name,
                    "class_data": class_data,
                }

            for class_name in class_out:
                self.write_class_data(
                    class_name,
                    class_out[class_name]["gen_data"],
                    self.opts.output_dir,
                    include_list,
                    class_out[class_name]["desired_name"],
                    class_out[class_name]["class_data"],
                    namespace=curr_nmspc,
                )

        files_to_include = set()
        all_gen_fun_data = list()
        mod_name = curr_nmspc if curr_nmspc else free_fun_name
        free_funs_dict = {}
        if "functions" in yaml_dict:
            free_funs_dict = yaml_dict["functions"]
            free_fun_incs = set()
            for fun_name, fun_data in free_funs_dict.items():
                gen_data_for_fun = pygccxml_data.free_functions(
                    lambda f: f.name == fun_name
                    and (
                        self.ensure_deps(f)
                        and self.ensure_templ_deps(f, free_fun_incs)
                    )
                )
                if gen_data_for_fun:
                    py_module_name = self.customizer.get_curr_mod_name(
                        fun_data
                    )
                    if py_module_name is None:
                        if mod_name == free_fun_name or not self.opts.ens_flag:
                            py_module_name = self.opts.module_name
                        else:
                            py_module_name = mod_name
                    eigen_str = ""
                    for fun_inst in gen_data_for_fun:
                        eigen_str = self.include_eigen(fun_inst, eigen_str)
                    if eigen_str:
                        self.namespace_web[py_module_name][
                            "files_to_include"
                        ].add(eigen_str)
                    self.namespace_web[py_module_name]["files_to_include"].add(
                        file_name
                    )
                    extra_incs = self.get_include_str(free_fun_incs)
                    if extra_incs:
                        self.namespace_web[py_module_name][
                            "files_to_include"
                        ].add(extra_incs)
                    self.namespace_web[py_module_name]["funs_dict"][
                        fun_name
                    ] = fun_data

                    self.namespace_web[py_module_name][
                        "all_gen_fun_data"
                    ].extend(gen_data_for_fun)

        all_gen_enum_data = list()
        enums_dict = {}
        if "enums" in yaml_dict:
            enums_dict = yaml_dict["enums"]
            for enum_name, enum_data in enums_dict.items():
                py_module_name = self.customizer.get_curr_mod_name(enum_data)
                if py_module_name is None:
                    if mod_name == free_fun_name or not self.opts.ens_flag:
                        py_module_name = self.opts.module_name
                    else:
                        py_module_name = mod_name
                self.namespace_web[py_module_name]["files_to_include"].add(
                    file_name
                )
                self.namespace_web[py_module_name]["enums_dict"][
                    enum_name
                ] = enum_data
                gen_data_for_enum = pygccxml_data.enumeration(enum_name)
                self.namespace_web[py_module_name]["all_gen_enum_data"].append(
                    gen_data_for_enum
                )

        # Now check for namespaces and recurse
        keys_left_to_check = set(yaml_dict.keys()) - {
            "classes",
            "functions",
            "enums",
            "customization",
        }
        for key in keys_left_to_check:
            if key in [x.name for x in pygccxml_data.namespaces()]:
                new_nmspc = self.add_namespace(curr_nmspc, key)
                for typedef in pygccxml_data.namespace(key).typedefs():
                    if "declaration" in dir(typedef.decl_type):
                        self.alias_data[
                            typedef.decl_type.declaration.decl_string
                        ] = typedef.decl_string.lstrip("::")
                self.generate_bindings(
                    yaml_dict[key],
                    pygccxml_data.namespace(key),
                    file_name=file_name,
                    curr_nmspc=new_nmspc,
                    free_fun_name=free_fun_name,
                )
            else:
                # Not a namespace, but keep looking anyway
                # First, check to see if a new file exists
                if key not in ["files", "namespaces"]:
                    file_name = key
                self.generate_bindings(
                    yaml_dict[key],
                    pygccxml_data,
                    file_name=file_name,
                    curr_nmspc=curr_nmspc,
                    free_fun_name=free_fun_name,
                )
        if not curr_nmspc:
            # write out all the non class_data for each namespace
            # need to do it all at once here rather than incrementally
            # as previous to handle complex mixed custom/c++ namespaces->modules
            # Write the free functions and enums to the same file
            for nmspc in self.namespace_web:
                self.write_non_class_data(
                    nmspc if nmspc != self.opts.module_name else free_fun_name,
                    self.namespace_web[nmspc]["all_gen_fun_data"],
                    self.namespace_web[nmspc]["all_gen_enum_data"],
                    self.opts.output_dir,
                    self.namespace_web[nmspc]["files_to_include"],
                    fun_yaml=self.namespace_web[nmspc]["funs_dict"],
                    enum_yaml=self.namespace_web[nmspc]["enums_dict"],
                    is_custom=self.namespace_web[nmspc]["is_custom"],
                )

    def template_args_to_underscores(self, name):
        if not name:
            return ""

        # First element is thing to replace, second is its substitute
        chars_to_replace = [("<", "_"), (", ", "_"), (">", ""), ("::", "_")]
        for old, new in chars_to_replace:
            name = name.replace(old, new)

        return name if len(name) > 1 else name[0]

    def generate_wrapper_cpp(self, module_info):
        includes_fmt = '#include "%s"\n'
        class_decs_fmt = "template class %s;\n"
        func_ptr_assign_fmt = "auto %s = &%s;\n"
        includes_str = "".join(
            [includes_fmt % fname for fname in module_info["to_include"]]
        )
        class_decs = "".join(
            class_decs_fmt % cname for cname in module_info["class_insts"]
        )
        # Functions are a bit longer
        func_list = list(module_info["func_insts"])
        for line in module_info["templ_members"]:
            class_decs += line + "\n"
        # Convert the templated class names to variable names
        # For example, foo<float, double> becomes foo_float_double
        var_names = [
            self.template_args_to_underscores(func_name)
            for func_name in func_list
        ]

        func_ptr_assigns = ""
        for var_name, func_name in zip(var_names, func_list):
            var_name = self.template_args_to_underscores(var_name)
            func_ptr_assigns += func_ptr_assign_fmt % (var_name, func_name)

        wrapper_cpp_file = os.path.join(
            self.opts.output_dir,
            "wrapper_{}.cpp".format(self.opts.module_name),
        )
        wrapper_cpp_text = self.opts.wrap_header_fmt.format(
            includes=includes_str,
            class_decs=class_decs,
            func_ptr_assigns=func_ptr_assigns,
        )
        self.write_data_to_file(wrapper_cpp_file, wrapper_cpp_text)

    def compile_and_parse_wrapper(self, module_info, rsp_includes, rsp_defs):
        # Need the castxml path at this point
        if self.opts.castxml_path is None:
            raise RuntimeError("ERROR: path to castxml executable not set!")

        castxml_config = pygccxml.parser.xml_generator_configuration_t(
            xml_generator_path=self.opts.castxml_path,
            xml_generator="castxml",
            cflags=rsp_defs,
            castxml_epic_version=1,
            include_paths=[self.opts.source_dir, self.opts.output_dir]
            + rsp_includes,
            start_with_declarations=module_info["all_qualified_names"],
        )

        # Run CastXML and parse back the resulting XML into a Python Object.
        pygccxml.utils.loggers.cxx_parser.setLevel(logging.CRITICAL)
        pygccxml.declarations.scopedef_t.RECURSIVE_DEFAULT = False
        pygccxml.declarations.scopedef_t.ALLOW_EMPTY_MDECL_WRAPPER = True
        if self.opts.generation_stage == "2":
            total = pygccxml.parser.parse(
                ["wrapper_{}.cpp".format(self.opts.module_name)],
                castxml_config,
                compilation_mode=pygccxml.parser.COMPILATION_MODE.ALL_AT_ONCE,
            )

            # Total seems to be a single item list, due to ALL_AT_ONCE mode, capture the data from
            # from the first item in the list
            total[0].init_optimizer()
            return total[0]

    def parse_and_generate(self):
        """
        Overall function to perform automatic generation of Pybind11 code from a C++ repository
        - Parses the yaml input
        - Generates a summary of the module
        - Writes instantiations and include into "wrapper.hpp", the only input to CastXML
        - Runs CastXML and uses pygccxml to read results into data object
        - Walks through yaml input, writing class/function/namespace data using the generated
          information from pygccxml
        :return: None
        """
        # init the pygccxml stuff
        # Adapted from CPPWG: https://github.com/jmsgrogan/cppwg/blob/265117455ed57eb250643a28ea6029c2bccf3ab3/cppwg/parsers/source_parser.py#L24
        # Source path is the directory with the yaml in it
        self.opts.source_dir = os.path.dirname(self.opts.yaml_path)
        # Module info to be populated by find_module_data
        module_info = {
            "all_qualified_names": set(),
            "to_include": set(),
            "templ_members": set(),
            "class_insts": list(),
            "func_insts": list(),
            "out_names": dict(),
            "indx_out": set(),
            "all_auto_bind": list(),
            "all_auto_bind_defs": set(),
            "non_template_classes": list(),
        }
        wrapper_yaml = yaml.safe_load(open(self.opts.yaml_path, "r"))
        # Generate a summary of the module
        self.find_module_data(
            wrapper_yaml,
            module_info,
            "{}_free_functions".format(self.opts.module_name),
            mod_tree=self.name_tree,
        )
        module_indexes = {self.opts.module_name: {}}
        for name, item in self.data_web.items():
            module_indexes[self.opts.module_name][name] = item
        module_index_file = os.path.join(
            self.opts.output_dir, self.opts.module_name + ".mdx"
        )
        with open(module_index_file, "w") as indx_file:
            json.dump(self.data_web, indx_file)
        # Short circuit: prints list of files to be generated by the run, if it were to continue.
        if self.opts.generation_stage == "1":
            file_name = posixpath.join(
                self.opts.output_dir, "%s.cpp" % self.opts.module_name
            )
            print(
                "%".join([file_name] + list(module_info["out_names"].keys()))
                + ";"
                + "%".join([module_index_file])
            )
            return

        rsp_includes, rsp_defs, rsp_mdx_files = self.clean_flags(
            self.opts.rsp_path
        )
        self.generate_wrapper_cpp(module_info)
        self.name_data = self.compile_and_parse_wrapper(
            module_info, rsp_includes, rsp_defs
        )

        for mdx_file in rsp_mdx_files:
            if mdx_file:
                self.load_mdx(mdx_file)
        if self.opts.generation_stage == "2":
            # Drop the namespace strings from the class_insts objects. This ensures they can be found in the classes
            # call below.
            classes_to_find = set(
                module_info["non_template_classes"]
                + [x.split("::")[-1] for x in module_info["class_insts"]]
            )
            classes = self.name_data.classes(
                lambda c: c.name in classes_to_find, recursive=True
            )
            bo = get_binding_order(classes)
            sorted_out_names = []
            for c in bo:
                # MDX strings don't have the leading ::
                remote_module = self.check_auto_bind(
                    c.decl_string.lstrip("::"), self.data_web
                )
                # In classes_to_find: part of current module and should have a future file
                if c.name in classes_to_find:
                    future_file_name = self.find_future_file_name(True, c.name)
                    if future_file_name not in sorted_out_names:
                        sorted_out_names.append(future_file_name)
                # Else: check to see if it's found in a linked module
                elif remote_module:
                    module_sig = self.opts.module_init_fun_signature_fmt.format(
                        module=remote_module
                    )
                    module_fwd = self.opts.module_init_fun_forward_fmt.format(
                        module=remote_module
                    )
                    if module_sig not in module_info["all_auto_bind"]:
                        module_info["all_auto_bind"].append(module_sig)
                    module_info["all_auto_bind_defs"].add(module_fwd)
                # Not found, warn them and add class to skiplist.
                else:
                    print(
                        "Warning: Class %s  was not found in current module or any linked module"
                        % c.name
                    )
                    self.skip_list.add(c.name)
            # Add the current module to the end of the list
            module_info["all_auto_bind"].append(
                self.opts.module_init_fun_signature_fmt.format(
                    module=self.opts.module_name
                )
            )
            module_info["modules"] = module_info["out_names"]
            module_info["out_names"] = (
                list(module_info["out_names"].keys() - set(sorted_out_names))
                + sorted_out_names
            )
            module_file = self.write_module_data(
                self.opts.module_name, module_info, self.opts.output_dir
            )
            self.generate_bindings(
                wrapper_yaml,
                self.name_data,
                free_fun_name="{}_free_functions".format(
                    self.opts.module_name
                ),
            )

    def check_auto_bind(self, name, data_web):
        found_module = name
        try:
            while not found_module.startswith("apb11"):
                found_module = data_web[found_module]
        except KeyError:
            return ""
        return found_module.replace("apb11_", "")


def main(argv=None):
    arg = ArgParser(config_file_parser_class=YAMLConfigFileParser)

    arg.add(
        "-o",
        "--output",
        action="store",
        dest="output_dir",
        required=False,
        default=os.getcwd(),
    )
    arg.add(
        "-y",
        "--input_yaml",
        action="store",
        dest="yaml_path",
        help="Path to input YAML file of objects to process",
        required=True,
    )
    arg.add(
        "--module_name",
        action="store",
        dest="module_name",
        help="Desired name of the output PyBind11 module",
        required=True,
    )
    arg.add(
        "-g",
        "--castxml-path",
        action="store",
        dest="castxml_path",
        help="Path to castxml",
        required=False,
    )
    arg.add(
        "-cg",
        "--config-path",
        dest="config_dir",
        required=False,
        is_config_file=True,
        help="config file path",
    )
    arg.add(
        "-s",
        "--stage",
        dest="generation_stage",
        required=True,
        choices=["1", "2"],
        action="store",
    )
    arg.add(
        "-rs", "--input_response", required=False, dest="rsp_path", default=""
    )
    arg.add(
        "-pm",
        "--private_members_as_fields",
        required=False,
        action="store",
        type=lambda x: bool(distutils.util.strtobool(x)),
        dest="pm_flag",
        default=False,
    )
    arg.add(
        "-twf",
        "--use_template_wrapping_function",
        required=False,
        action="store",
        type=lambda x: bool(distutils.util.strtobool(x)),
        dest="twf_flag",
        default=False,
    )
    arg.add(
        "-ens",
        "--enforce_namespace_structure",
        required=False,
        action="store",
        type=lambda x: bool(distutils.util.strtobool(x)),
        dest="ens_flag",
        default=True,
    )
    arg.add(
        "-agns",
        "--apply_global_namespace",
        required=False,
        action="store",
        type=lambda x: bool(distutils.util.strtobool(x)),
        dest="gns_flag",
        default=True,
    )
    arg.add(
        "-stdexdec",
        "--expand_stl_declaration",
        required=False,
        action="store",
        type=lambda x: bool(distutils.util.strtobool(x)),
        dest="stdexdec_flag",
        default=True,
    )
    arg.add(
        "-aq",
        "--assumed_qualifiers",
        required=False,
        action="append",
        dest="aq",
        default=[],
    )
    arg.add(
        "-td",
        "--template_defaults",
        required=False,
        action="append",
        dest="td",
        default=[],
    )
    arg.add(
        "-exdec",
        "--expand_declarations",
        required=False,
        action="store",
        type=lambda x: bool(distutils.util.strtobool(x)),
        dest="exdec",
        default=True,
    )
    arg.add(
        "-eds",
        "--enable_doc_strings",
        required=False,
        action="store",
        type=lambda x: bool(distutils.util.strtobool(x)),
        dest="eds_flag",
        default=True,
    )
    arg.add(
        "-ppw",
        "--print_python_warnings",
        required=False,
        action="store",
        type=lambda x: bool(distutils.util.strtobool(x)),
        dest="pw_flag",
        default=True,
    )
    arg.add(
        "-tpm",
        "--top_python_module",
        dest="top_module",
        required=False,
        action="store",
        default=None,
        help="Name of module this python package will be added to",
    )
    arg.add(
        "--no_format",
        dest="skip_formatting",
        action="store_true",
        default=False,
        help="Do not attempt to use clang-format to format the output files",
    )
    arg.add(
        "--compiler_opts",
        dest="clang_opts",
        action="store",
        default=None,
        help="Specify additional compiler arguments to be passed to Clang/LLVM via CastXML",
    )
    arg.add(
        "--pass_eigen_by_ref",
        "-epbr",
        dest="eigen_ref",
        action="store",
        type=lambda x: bool(distutils.util.strtobool(x)),
        default=True,
        help="Toogle the use of Eigen PBR via Eigen::Ref<MarixType>",
    )
    arg.add(
        "--use_eigen_return_policy",
        "-erp",
        dest="eigen_ret_pol",
        action="store",
        type=lambda x: bool(distutils.util.strtobool(x)),
        default=True,
        help="Toggle use of return policy for returning Eigen types",
    )
    arg.add(
        "--use_cxx_14",
        "-cxx14",
        dest="cxx14_flag",
        action="store",
        type=lambda x: bool(distutils.util.strtobool(x)),
        default=True,
        help="Enable cxx14 standard features, if turned off, cxx11 is assumed",
    )
    # The formatted strings that will write the pybind code are also configurable
    arg.add(
        "--common_cpp_body_fmt", required=False, default=tb.common_cpp_body
    )
    arg.add(
        "--submodule_signature_fmt",
        required=False,
        default=tb.submodule_signature,
    )
    arg.add(
        "--class_info_body_fmt", required=False, default=tb.class_info_body
    )
    arg.add(
        "--init_fun_signature_fmt",
        required=False,
        default=tb.init_fun_signature,
    )
    arg.add(
        "--init_fun_forward_fmt", required=False, default=tb.init_fun_forward
    )
    arg.add(
        "--init_fun_forward_non_void_fmt",
        required=False,
        default=tb.init_fun_forward_non_void,
    )
    arg.add("--cppbody_fmt", type=str, required=False, default=tb.cppbody)
    arg.add(
        "--class_module_cpp_fmt", required=False, default=tb.class_module_cpp
    )
    arg.add(
        "--non_class_module_cpp_fmt",
        required=False,
        default=tb.non_class_module_cpp,
    )
    arg.add(
        "--non_class_module_return_cpp_fmt",
        required=False,
        default=tb.non_class_module_return_cpp,
    )
    arg.add("--member_func_fmt", required=False, default=tb.member_func)
    arg.add("--constructor_fmt", required=False, default=tb.constructor)
    arg.add(
        "--member_func_arg_fmt", required=False, default=tb.member_func_arg
    )
    arg.add(
        "--public_member_var_fmt", required=False, default=tb.public_member_var
    )
    arg.add(
        "--private_member_var_fmt",
        required=False,
        default=tb.private_member_var,
    )
    arg.add(
        "--member_reference_fmt", required=False, default=tb.member_reference
    )
    arg.add(
        "--overload_template_fmt", required=False, default=tb.overload_template
    )
    arg.add("--wrap_header_fmt", required=False, default=tb.wrap_header)
    arg.add("--operator_fmt", required=False, default=tb.operator_template)
    arg.add("--call_operator_fmt", required=False, default=tb.call_template)
    arg.add("--enum_header_fmt", required=False, default=tb.enum_header)
    arg.add("--enum_val_fmt", required=False, default=tb.enum_val)
    arg.add("--tramp_override_fmt", required=False, default=tb.tramp_override)
    arg.add("--trampoline_def_fmt", required=False, default=tb.trampoline_def)
    arg.add(
        "--pybind_overload_macro_args_fmt",
        required=False,
        default=tb.pybind_overload_macro_args,
    )
    arg.add(
        "--copy_constructor_tramp_fmt",
        required=False,
        default=tb.copy_constructor_tramp,
    )
    arg.add(
        "--publicist_using_directives_fmt",
        required=False,
        default=tb.publicist_using_directives,
    )
    arg.add("--publicist_def_fmt", required=False, default=tb.publicist_def)
    arg.add(
        "--module_init_fun_forward_fmt",
        required=False,
        default=tb.module_init_fun_forward,
    )
    arg.add(
        "--module_init_fun_signature_fmt",
        required=False,
        default=tb.module_init_fun_signature,
    )
    arg.add(
        "--submodule_init_fun_forward_fmt",
        required=False,
        default=tb.submodule_init_fun_forward,
    )
    arg.add(
        "--submodule_init_fun_signature_fmt",
        required=False,
        default=tb.submodule_init_fun_signature,
    )
    arg.add(
        "--submodule_init_fun_forward_non_void_fmt",
        required=False,
        default=tb.submodule_init_fun_forward_non_void,
    )
    arg.add(
        "--submodule_init_fun_signature_return_fmt",
        required=False,
        default=tb.submodule_init_fun_signature_return,
    )
    arg.add(
        "--repr_function_fmt", required=False, default=tb.repr_function,
    )
    arg.add(
        "--submodule_return", required=False, default=tb.submodule_return,
    )
    arg.add(
        "--nullptr_arg_val_fmt", required=False, default=tb.nullptr_arg_val,
    )
    arg.add(
        "--arg_val_cast_fmt", required=False, default=tb.arg_val_cast,
    )
    arg.add("--module_local_fmt", required=False, default=tb.module_local)
    arg.add(
        "--keep_alive_fmt", required=False, default=tb.keep_alive,
    )
    arg.add(
        "--generic_lambda_fmt", required=False, default=tb.generic_lambda,
    )
    arg.add(
        "--return_policy_fmt", required=False, default=tb.return_policy,
    )
    arg.add(
        "--matrixDRef", required=False, default=tb.matrixDRef,
    )
    arg.add(
        "--shared_holder", required=False, default=tb.shared_holder,
    )
    arg.add(
        "--unique_holder", required=False, default=tb.unique_holder,
    )
    arg.add("--attribute_fmt", required=False, default=tb.attribute)
    arg.add("--custom_ctor_fmt", required=False, default=tb.custom_constructor)
    arg.add("--make_unique_fmt", required=False, default=tb.make_unique)
    arg.add("--make_shared_fmt", required=False, default=tb.make_shared)
    arg.add("--compose_unique", required=False, default=tb.compose_unique)
    arg.add("--conversion", required=False, default=tb.conversion)
    arg.add(
        "--generic_lambda_def_fmt",
        required=False,
        default=tb.generic_lambda_def,
    )
    arg.add("--generic_return_fmt", required=False, default=tb.generic_return)
    arg.add("--move_fmt", required=False, default=tb.std_move)
    arg.add("--make_tuple_fmt", required=False, default=tb.make_tuple)
    arg.add(
        "--template_setup_fn_fmt", required=False, default=tb.template_setup_fn
    )
    arg.add(
        "--class_init_wrapper_fmt",
        required=False,
        default=tb.class_init_wrapper,
    )
    arg.add(
        "--class_init_call_fmt", required=False, default=tb.class_init_call
    )
    options = arg.parse_args(argv)
    # CastXML Check for a relatively recent version
    rtn = subprocess.check_output([options.castxml_path, "--version"])
    if not options.pw_flag:
        warnings.simplefilter("ignore")
    if re.search(b"castxml version 0.4.[0-9]+", rtn):
        BindingsGenerator(options).parse_and_generate()
    else:
        print(
            "CastXML version is incompatible.  Please install CastXML 0.4.* by PIP \
              or download a version from https://data.kitware.com/#folder/57b5de948d777f10f2696370"
        )


if __name__ == "__main__":
    main()
